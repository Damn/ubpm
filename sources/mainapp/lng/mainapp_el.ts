<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="el">
<context>
    <name>DialogAbout</name>
    <message>
        <location filename="../DialogAbout.ui" line="14"/>
        <source>About UBPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="41"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="53"/>
        <source>Universal Blood Pressure Manager</source>
        <translation type="unfinished">Καθολικός διευθυντής πίεσης αίματος</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="91"/>
        <source>Qt Framework</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="129"/>
        <source>Operating System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="220"/>
        <source>Environment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="231"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="269"/>
        <source>Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="307"/>
        <source>Cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="345"/>
        <source>Guides</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="383"/>
        <source>Languages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="421"/>
        <source>Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="459"/>
        <source>Themes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="490"/>
        <source>Translators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.cpp" line="14"/>
        <source>The program is provided as is with no warranty of any kind, including the warranty of design, merchantability and fitness for a particular purpose.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="14"/>
        <source>Data Analysis</source>
        <translation>Ανάλυση Δεδομένων</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="24"/>
        <source>Query</source>
        <translation>Ερώτημα</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="43"/>
        <location filename="../DialogAnalysis.cpp" line="209"/>
        <source>Results</source>
        <translation>Αποτελέσματα</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="82"/>
        <source>Date</source>
        <translation>Ημερομηνία</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="91"/>
        <source>Time</source>
        <translation>Ώρα</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="100"/>
        <source>Systolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="109"/>
        <source>Diastolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="118"/>
        <source>P.Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="127"/>
        <source>Heartrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="136"/>
        <source>Irregular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="145"/>
        <source>Movement</source>
        <translation>Κίνηση</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="154"/>
        <source>Invisible</source>
        <translation>Μη ορατό</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="163"/>
        <source>Comment</source>
        <translation>Σχόλιο</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="193"/>
        <source>Close</source>
        <translation>Κλείσετε</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="51"/>
        <source>Could not create memory database!

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>No results for this query found!</source>
        <translation>Δεν βρέθηκαν αποτελέσματα για αυτό το ερώτημα!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 για %2 [ %3 %4 | %5 %6 | %7% ]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Αποτέλεσμα</numerusform>
            <numerusform>Αποτελέσματα</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Match(es)</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Record(s)</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>User %1</source>
        <translation>Χρήστης %1</translation>
    </message>
</context>
<context>
    <name>DialogDistribution</name>
    <message>
        <location filename="../DialogDistribution.ui" line="20"/>
        <source>Data Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="77"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="88"/>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="99"/>
        <source>Close</source>
        <translation type="unfinished">Κλείσετε</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogDistribution.cpp" line="18"/>
        <source>Record(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="119"/>
        <source>Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="121"/>
        <source>Optimal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="123"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="125"/>
        <source>High Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="127"/>
        <source>Hyper 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="129"/>
        <source>Hyper 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="131"/>
        <source>Hyper 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="148"/>
        <source>SYS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="160"/>
        <source>DIA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="171"/>
        <source>Blood Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="210"/>
        <source>Athlete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="212"/>
        <source>Excellent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="214"/>
        <source>Great</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="216"/>
        <source>Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="218"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="220"/>
        <source>Below Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="222"/>
        <source>Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="239"/>
        <source>BPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="265"/>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="346"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="347"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="352"/>
        <source>%1 (Age: %2, Height: %3cm, Weight: %4Kg, BMI: %5)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="352"/>
        <source>User %1</source>
        <translation type="unfinished">Χρήστης %1</translation>
    </message>
</context>
<context>
    <name>DialogDonation</name>
    <message>
        <location filename="../DialogDonation.ui" line="14"/>
        <source>Donation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="67"/>
        <source>Copy E-Mail and open Amazon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="154"/>
        <source>Open PayPal (+)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="171"/>
        <source>Open PayPal (-)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="46"/>
        <source>If you have an Amazon account, you can send money by gift card.

Click the button below to open the Amazon website in your browser and

- sign in to your account
- select or enter the amount you would like to send
- select e-mail delivery
- paste the e-mail address as the recipient
- add a message
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="124"/>
        <source>If you have a PayPal account, you can send money using the &quot;Friends and Family&quot; feature.

Open your PayPal app and

- search for @LazyT (Thomas Löwe)
- enter the amount you would like to send
- add a message
- complete the donation

You can also click the first button below to do this in your browser without using the app.

If you do not have a PayPal account but do have a credit/debit card click the second button below to go to the PayPal donation page and

- select onetime or repeating donation
- select or enter the amount you would like to send
- optionally check the box to cover the fees
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="230"/>
        <source>If you are in the Euro Payments Area, you can transfer money from your bank account.

Click the buttons below to copy the name/IBAN/BIC to the clipboard and

- sign in to your bank account
- paste the name/IBAN/BIC into the transfer form
- enter the amount you would like to send
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="251"/>
        <source>Copy Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="268"/>
        <source>Copy IBAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="285"/>
        <source>Copy BIC</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Οδηγός</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="61"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="53"/>
        <source>%1 guide not found!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogMigration</name>
    <message>
        <location filename="../DialogMigration.ui" line="14"/>
        <source>Data Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="24"/>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="72"/>
        <source>Open Data Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="97"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="217"/>
        <source>Date Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="267"/>
        <source>Irregular Detection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="167"/>
        <source>Startline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="183"/>
        <source>Elements per Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="111"/>
        <source>Delimiter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="303"/>
        <source>Time Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="199"/>
        <source>Movement Detection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="239"/>
        <source>Date Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="344"/>
        <source>Positions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="488"/>
        <source>Irregular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="504"/>
        <source>Date</source>
        <translation type="unfinished">Ημερομηνία</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="452"/>
        <source>Systolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="410"/>
        <source>Heartrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="394"/>
        <source>Movement</source>
        <translation type="unfinished">Κίνηση</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="329"/>
        <source>Ignore Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="368"/>
        <source>Time</source>
        <translation type="unfinished">Ώρα</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="436"/>
        <source>Diastolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="550"/>
        <source>Comment</source>
        <translation type="unfinished">Σχόλιο</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="567"/>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="786"/>
        <source>Migrate User 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="815"/>
        <source>Migrate User 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="841"/>
        <source>Copy to Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="861"/>
        <source>Select predefined settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="895"/>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="906"/>
        <source>Test Data Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="920"/>
        <source>Start Data Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="251"/>
        <source>Choose Data Source for Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="251"/>
        <source>CSV File (*.csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="304"/>
        <source>%L1 Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="305"/>
        <source>%1 Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="319"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="341"/>
        <source>Startline can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="347"/>
        <source>Startline must be smaller than the number of lines.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="355"/>
        <source>Line %1 = %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="360"/>
        <source>Delimiter can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="367"/>
        <source>Startline doesn&apos;t contain &quot;%1&quot; delimiter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="378"/>
        <source>Elements can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="384"/>
        <source>Elements can&apos;t be smaller than 4.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="390"/>
        <source>Elements doesn&apos;t match (found %1).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="397"/>
        <source>Date format can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="403"/>
        <source>Single date format must also contain a time format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="410"/>
        <source>Date position can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="416"/>
        <source>Date position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="423"/>
        <source>Time position requires also a time parameter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="429"/>
        <source>Time format requires also a time position.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="435"/>
        <source>Time position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="442"/>
        <source>Systolic position can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="448"/>
        <source>Systolic position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="455"/>
        <source>Diastolic position can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="461"/>
        <source>Diastolic position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="468"/>
        <source>Heartrate position can&apos;t be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="474"/>
        <source>Heartrate position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="481"/>
        <source>Irregular position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="487"/>
        <source>Irregular position requires also an irregular parameter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="494"/>
        <source>Movement position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="500"/>
        <source>Movement position requires also a movement parameter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="507"/>
        <source>Comment position must be smaller than elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="523"/>
        <source>Positions can&apos;t exist twice.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="539"/>
        <source>Date format is invalid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="546"/>
        <source>Time format is invalid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="556"/>
        <source>Date/Time format is invalid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="747"/>
        <source>Successfully migrated %n record(s) for user %1.</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="751"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="756"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Μη αυτόματη εγγραφή</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Εγγραφή δεδομένων</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="211"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Προσθήκη εγγραφής για %1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Επιλέξτε ημερομηνία και ώρα</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="148"/>
        <source>Enter DIA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="167"/>
        <source>Enter BPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="186"/>
        <source>Movement</source>
        <translation>Κίνηση</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="239"/>
        <source>Enter Optional Comment</source>
        <translation>Εισαγάγετε προαιρετικό σχόλιο</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="269"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Εμφάνιση μηνύματος για επιτυχημένη δημιουργία / διαγραφή / τροποποίηση εγγραφής</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="287"/>
        <source>Delete</source>
        <translation>Διαγράψετε</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="304"/>
        <source>Create</source>
        <translation>Δημιουργία</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="324"/>
        <source>Close</source>
        <translation>Κλείσετε</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Χρήστης 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Χρήστης 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="34"/>
        <location filename="../DialogRecord.cpp" line="187"/>
        <source>Modify</source>
        <translation>Τροποποίηση</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="132"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="138"/>
        <source>Data record successfully deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="150"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="160"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="169"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="191"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="197"/>
        <source>Data Record successfully modified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="206"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="212"/>
        <source>Data Record successfully created.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.cpp" line="167"/>
        <source>Choose Database Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="177"/>
        <source>Choose Database Backup Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="211"/>
        <source>Could not start pdf viewer!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="216"/>
        <source>Could not open manual &quot;%1&quot;!

%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="339"/>
        <source>The selected database already exists.

Please choose the preferred action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="341"/>
        <source>Overwrite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="342"/>
        <source>Merge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="343"/>
        <source>Swap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="345"/>
        <source>Overwrite:

Overwrites the selected database with the current database.

Merge:

Merges the selected database into the current database.

Swap:

Deletes the current database and use the selected database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="353"/>
        <source>The current database is empty.

The selected database will be erased on exit, if no data will be added.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="383"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="390"/>
        <source>The database backup should be located on a different hard disk, partition or directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="395"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="402"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="409"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="416"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="424"/>
        <source>Please enable symbols or lines for chart!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="431"/>
        <source>Please enter a valid e-mail address!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="438"/>
        <source>Please enter a e-mail subject!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="445"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="556"/>
        <source>User 1</source>
        <translation type="unfinished">Χρήστης 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="570"/>
        <source>User 2</source>
        <translation type="unfinished">Χρήστης 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="683"/>
        <source>Blood Pressure Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="684"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="726"/>
        <source>Abort setup and discard all changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="52"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="58"/>
        <source>Current Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="74"/>
        <source>Change Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="100"/>
        <source>Encrypt With SQLCipher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="103"/>
        <source>Encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="115"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="128"/>
        <source>Show Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="282"/>
        <source>User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="317"/>
        <location filename="../DialogSettings.ui" line="580"/>
        <source>Male</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="346"/>
        <location filename="../DialogSettings.ui" line="609"/>
        <source>Female</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="442"/>
        <location filename="../DialogSettings.ui" line="714"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="311"/>
        <location filename="../DialogSettings.ui" line="574"/>
        <source>Mandatory Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="372"/>
        <location filename="../DialogSettings.ui" line="644"/>
        <source>Age Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="458"/>
        <location filename="../DialogSettings.ui" line="730"/>
        <source>Additional Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="518"/>
        <location filename="../DialogSettings.ui" line="790"/>
        <source>Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="547"/>
        <location filename="../DialogSettings.ui" line="819"/>
        <source>Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="844"/>
        <source>Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="850"/>
        <location filename="../DialogSettings.cpp" line="70"/>
        <source>Import Plugins [ %1 ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="880"/>
        <source>Show Device Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="903"/>
        <source>Show Device Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="948"/>
        <source>Open Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="961"/>
        <source>Maintainer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="989"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="996"/>
        <source>Send E-Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1009"/>
        <source>Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1016"/>
        <source>Producer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1033"/>
        <source>Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1160"/>
        <source>X-Axis Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1166"/>
        <source>Dynamic Scaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1147"/>
        <source>Healthy Ranges</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1064"/>
        <source>Symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1141"/>
        <source>Colored Areas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="865"/>
        <source>Please choose Device Plugin…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1288"/>
        <location filename="../DialogSettings.ui" line="1563"/>
        <source>Systolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1376"/>
        <location filename="../DialogSettings.ui" line="1651"/>
        <source>Diastolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1464"/>
        <location filename="../DialogSettings.ui" line="1739"/>
        <source>Heartrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1836"/>
        <source>Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1921"/>
        <location filename="../DialogSettings.ui" line="2104"/>
        <source>Systolic Warnlevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1867"/>
        <location filename="../DialogSettings.ui" line="2164"/>
        <source>Diastolic Warnlevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="153"/>
        <source>Automatic Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="156"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="173"/>
        <source>Current Backup Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="186"/>
        <source>Change Backup Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="213"/>
        <source>Backup Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="223"/>
        <source>Daily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="232"/>
        <source>Weekly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="241"/>
        <source>Monthly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="253"/>
        <source>Keep Copies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="486"/>
        <location filename="../DialogSettings.ui" line="758"/>
        <source>Birthday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1073"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1097"/>
        <location filename="../DialogSettings.ui" line="1125"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1179"/>
        <source>Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1188"/>
        <location filename="../DialogSettings.ui" line="1216"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1245"/>
        <source>Show Heartrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1251"/>
        <source>Show Heartrate in Chart View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1261"/>
        <source>Print Heartrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1267"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1981"/>
        <location filename="../DialogSettings.ui" line="2218"/>
        <source>P.Pressure Warnlevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2035"/>
        <location filename="../DialogSettings.ui" line="2272"/>
        <source>Heartrate Warnlevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2337"/>
        <source>Statistic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2343"/>
        <source>Bar Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2349"/>
        <source>Show Median instead of Average Bars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2359"/>
        <source>Legend Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2365"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2380"/>
        <source>E-Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2388"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2407"/>
        <source>Subject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2425"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2446"/>
        <source>Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2452"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2458"/>
        <source>Log Device Communication to File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2465"/>
        <source>Import Measurements automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2475"/>
        <source>Bluetooth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2481"/>
        <source>Discover Device automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2488"/>
        <source>Connect Device automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2503"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2509"/>
        <source>Autostart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2515"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2528"/>
        <source>Notification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2534"/>
        <source>Always show Result after Online Update Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2550"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2567"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2584"/>
        <source>Close</source>
        <translation type="unfinished">Κλείσετε</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="178"/>
        <location filename="../DialogUpdate.ui" line="199"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="219"/>
        <source>Ignore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="183"/>
        <source>No new version found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="42"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="73"/>
        <source>Downloading update failed!

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="73"/>
        <source>Checking update failed!

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="156"/>
        <source>Unexpected response from update server!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="173"/>
        <source>*%1 not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="203"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="207"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="220"/>
        <source>Could not start new version!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="247"/>
        <source>The program was installed by Flatpak.

Please update using the Flatpak internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="253"/>
        <source>The program was installed by Snap.

Please update using the Snap internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="259"/>
        <source>The program was installed from distribution.

Please update using the operating systems internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="265"/>
        <source>The program was installed from source code.

Please update the sources, rebuild and reinstall manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="291"/>
        <source>Really abort download?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="227"/>
        <source>Could not save update to %1!

%2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="14"/>
        <source>Universal Blood Pressure Manager</source>
        <translation type="unfinished">Καθολικός διευθυντής πίεσης αίματος</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="30"/>
        <source>Show Previous Period</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="104"/>
        <source>Show Next Period</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <source>Date</source>
        <translation type="unfinished">Ημερομηνία</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Time</source>
        <translation type="unfinished">Ώρα</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="159"/>
        <location filename="../MainWindow.cpp" line="4554"/>
        <source>Systolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="160"/>
        <location filename="../MainWindow.cpp" line="4555"/>
        <source>Diastolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation type="unfinished">Κίνηση</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation type="unfinished">Μη ορατό</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <source>Comment</source>
        <translation type="unfinished">Σχόλιο</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>Weekly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Monthly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Quarterly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>½ Yearly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>Yearly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="793"/>
        <source>Last 7 Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="828"/>
        <source>Last 14 Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="860"/>
        <source>Last 21 Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="892"/>
        <source>Last 28 Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="924"/>
        <source>Last 3 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="956"/>
        <source>Last 6 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="988"/>
        <source>Last 9 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1020"/>
        <source>Last 12 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1052"/>
        <source>All Records</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1099"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1103"/>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1125"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1141"/>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1145"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1154"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1163"/>
        <source>Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1172"/>
        <source>Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1197"/>
        <source>Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1201"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1213"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1228"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1317"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1320"/>
        <location filename="../MainWindow.ui" line="1323"/>
        <source>Quit Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1335"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1338"/>
        <location filename="../MainWindow.ui" line="1341"/>
        <source>About Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1350"/>
        <source>Guide</source>
        <translation type="unfinished">Οδηγός</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1353"/>
        <location filename="../MainWindow.ui" line="1356"/>
        <source>Show Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1365"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1368"/>
        <location filename="../MainWindow.ui" line="1371"/>
        <source>Check Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1530"/>
        <source>To PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1533"/>
        <location filename="../MainWindow.ui" line="1536"/>
        <source>Export To PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1545"/>
        <source>Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1548"/>
        <location filename="../MainWindow.ui" line="1551"/>
        <source>Migrate from Vendor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1795"/>
        <source>Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1798"/>
        <location filename="../MainWindow.ui" line="1801"/>
        <source>Contribute Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1810"/>
        <source>E-Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1813"/>
        <location filename="../MainWindow.ui" line="1816"/>
        <source>Send E-Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1828"/>
        <source>Icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1831"/>
        <location filename="../MainWindow.ui" line="1834"/>
        <source>Change Icon Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1845"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1848"/>
        <location filename="../MainWindow.ui" line="1851"/>
        <source>System Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1859"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1862"/>
        <location filename="../MainWindow.ui" line="1865"/>
        <source>Light Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1873"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1876"/>
        <location filename="../MainWindow.ui" line="1879"/>
        <source>Dark Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1888"/>
        <source>Donation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1903"/>
        <source>Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1906"/>
        <location filename="../MainWindow.ui" line="1909"/>
        <source>Show Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="161"/>
        <location filename="../MainWindow.cpp" line="4556"/>
        <source>Heartrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1891"/>
        <location filename="../MainWindow.ui" line="1894"/>
        <source>Make Donation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1383"/>
        <source>Bugreport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1386"/>
        <location filename="../MainWindow.ui" line="1389"/>
        <source>Send Bugreport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1398"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1401"/>
        <location filename="../MainWindow.ui" line="1404"/>
        <source>Change Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1419"/>
        <source>From Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1422"/>
        <location filename="../MainWindow.ui" line="1425"/>
        <source>Import From Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1437"/>
        <source>From File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1440"/>
        <location filename="../MainWindow.ui" line="1443"/>
        <source>Import From File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1452"/>
        <source>From Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1455"/>
        <location filename="../MainWindow.ui" line="1458"/>
        <source>Import From Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1467"/>
        <source>To CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1470"/>
        <location filename="../MainWindow.ui" line="1473"/>
        <source>Export To CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1485"/>
        <source>To XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1488"/>
        <location filename="../MainWindow.ui" line="1491"/>
        <source>Export To XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1500"/>
        <source>To JSON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1503"/>
        <location filename="../MainWindow.ui" line="1506"/>
        <source>Export To JSON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1515"/>
        <source>To SQL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1518"/>
        <location filename="../MainWindow.ui" line="1521"/>
        <source>Export To SQL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1563"/>
        <source>Print Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1566"/>
        <location filename="../MainWindow.ui" line="1569"/>
        <source>Print Chart View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1578"/>
        <source>Print Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1581"/>
        <location filename="../MainWindow.ui" line="1584"/>
        <source>Print Table View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1593"/>
        <source>Print Statistic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1596"/>
        <location filename="../MainWindow.ui" line="1599"/>
        <source>Print Statistic View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1608"/>
        <source>Preview Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1611"/>
        <location filename="../MainWindow.ui" line="1614"/>
        <source>Preview Chart View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1623"/>
        <source>Preview Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1626"/>
        <location filename="../MainWindow.ui" line="1629"/>
        <source>Preview Table View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1638"/>
        <source>Preview Statistic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1641"/>
        <location filename="../MainWindow.ui" line="1644"/>
        <source>Preview Statistic View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1653"/>
        <location filename="../MainWindow.ui" line="1656"/>
        <location filename="../MainWindow.ui" line="1659"/>
        <source>Clear All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1671"/>
        <location filename="../MainWindow.ui" line="1674"/>
        <location filename="../MainWindow.ui" line="1677"/>
        <source>Clear User 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1686"/>
        <location filename="../MainWindow.ui" line="1689"/>
        <location filename="../MainWindow.ui" line="1692"/>
        <source>Clear User 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1752"/>
        <source>Analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1755"/>
        <location filename="../MainWindow.ui" line="1758"/>
        <source>Analyze Records</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1777"/>
        <location filename="../MainWindow.ui" line="1780"/>
        <location filename="../MainWindow.ui" line="1783"/>
        <source>Time Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="108"/>
        <location filename="../MainWindow.cpp" line="109"/>
        <location filename="../MainWindow.cpp" line="4524"/>
        <location filename="../MainWindow.cpp" line="4525"/>
        <source>Records For Selected User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="116"/>
        <location filename="../MainWindow.cpp" line="117"/>
        <location filename="../MainWindow.cpp" line="4527"/>
        <location filename="../MainWindow.cpp" line="4528"/>
        <source>Select Date &amp; Time</source>
        <translation type="unfinished">Επιλέξτε ημερομηνία και ώρα</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="169"/>
        <location filename="../MainWindow.cpp" line="4558"/>
        <source>Systolic - Value Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="170"/>
        <location filename="../MainWindow.cpp" line="4559"/>
        <source>Diastolic - Value Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="173"/>
        <location filename="../MainWindow.cpp" line="4561"/>
        <source>Systolic - Target Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="174"/>
        <location filename="../MainWindow.cpp" line="4562"/>
        <source>Diastolic - Target Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1617"/>
        <source>Athlete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1618"/>
        <source>Excellent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1618"/>
        <source>Optimal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1619"/>
        <source>Great</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1620"/>
        <source>Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1619"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1713"/>
        <location filename="../MainWindow.ui" line="1716"/>
        <location filename="../MainWindow.ui" line="1737"/>
        <location filename="../MainWindow.ui" line="1740"/>
        <location filename="../MainWindow.cpp" line="102"/>
        <location filename="../MainWindow.cpp" line="103"/>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="4256"/>
        <location filename="../MainWindow.cpp" line="4257"/>
        <location filename="../MainWindow.cpp" line="4258"/>
        <location filename="../MainWindow.cpp" line="4259"/>
        <location filename="../MainWindow.cpp" line="4588"/>
        <location filename="../MainWindow.cpp" line="4589"/>
        <location filename="../MainWindow.cpp" line="4590"/>
        <location filename="../MainWindow.cpp" line="4591"/>
        <source>Switch To %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="102"/>
        <location filename="../MainWindow.cpp" line="103"/>
        <location filename="../MainWindow.cpp" line="587"/>
        <location filename="../MainWindow.cpp" line="4256"/>
        <location filename="../MainWindow.cpp" line="4257"/>
        <location filename="../MainWindow.cpp" line="4588"/>
        <location filename="../MainWindow.cpp" line="4589"/>
        <source>User 1</source>
        <translation type="unfinished">Χρήστης 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="597"/>
        <location filename="../MainWindow.cpp" line="4258"/>
        <location filename="../MainWindow.cpp" line="4259"/>
        <location filename="../MainWindow.cpp" line="4590"/>
        <location filename="../MainWindow.cpp" line="4591"/>
        <source>User 2</source>
        <translation type="unfinished">Χρήστης 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="171"/>
        <location filename="../MainWindow.cpp" line="4560"/>
        <source>Heartrate - Value Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="175"/>
        <location filename="../MainWindow.cpp" line="4563"/>
        <source>Heartrate - Target Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="341"/>
        <source>No active device plugin, auto import aborted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="448"/>
        <source>Could not create backup directory.

Please check backup location setting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="477"/>
        <location filename="../MainWindow.cpp" line="500"/>
        <source>Could not backup database:

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="490"/>
        <source>Could not delete outdated backup:

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="517"/>
        <source>Could not register icons.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="650"/>
        <source>Blood Pressure Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="651"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1129"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1134"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1617"/>
        <source>Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1620"/>
        <source>High Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1621"/>
        <location filename="../MainWindow.cpp" line="2088"/>
        <location filename="../MainWindow.cpp" line="4567"/>
        <location filename="../MainWindow.cpp" line="4571"/>
        <location filename="../MainWindow.cpp" line="4575"/>
        <location filename="../MainWindow.h" line="165"/>
        <location filename="../MainWindow.h" line="169"/>
        <location filename="../MainWindow.h" line="173"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1621"/>
        <source>Hyper 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1622"/>
        <source>Below Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1622"/>
        <source>Hyper 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1623"/>
        <source>Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1623"/>
        <source>Hyper 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1768"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1781"/>
        <location filename="../MainWindow.cpp" line="1803"/>
        <location filename="../MainWindow.cpp" line="4534"/>
        <source>Switch Language to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1828"/>
        <location filename="../MainWindow.cpp" line="1843"/>
        <location filename="../MainWindow.cpp" line="4542"/>
        <source>Switch Theme to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1868"/>
        <location filename="../MainWindow.cpp" line="1888"/>
        <location filename="../MainWindow.cpp" line="4550"/>
        <source>Switch Style to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1959"/>
        <location filename="../MainWindow.cpp" line="1984"/>
        <source>Edit record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2088"/>
        <location filename="../MainWindow.cpp" line="4568"/>
        <location filename="../MainWindow.cpp" line="4572"/>
        <location filename="../MainWindow.cpp" line="4576"/>
        <location filename="../MainWindow.h" line="166"/>
        <location filename="../MainWindow.h" line="170"/>
        <location filename="../MainWindow.h" line="174"/>
        <source>Median</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2090"/>
        <source>Click to swap Average and Median</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2185"/>
        <source>Click to swap Legend and Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2262"/>
        <source>No records to preview for selected time range!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2294"/>
        <source>No records to print for selected time range!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2340"/>
        <location filename="../MainWindow.cpp" line="2390"/>
        <location filename="../MainWindow.cpp" line="2466"/>
        <location filename="../MainWindow.cpp" line="2597"/>
        <source>User %1</source>
        <translation type="unfinished">Χρήστης %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>DATE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>TIME</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>SYS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>DIA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>BPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>IHB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>COMMENT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>PPR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>MOV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2662"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3413"/>
        <location filename="../MainWindow.cpp" line="4164"/>
        <source>Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3413"/>
        <location filename="../MainWindow.cpp" line="4181"/>
        <source>Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3413"/>
        <location filename="../MainWindow.cpp" line="4198"/>
        <source>Statistic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4122"/>
        <source>No records to analyse!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4134"/>
        <source>No records to display for selected time range!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4216"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4226"/>
        <source>Could not start e-mail client!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4231"/>
        <source>No records to e-mail for selected time range!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4237"/>
        <source>Select Icon Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4243"/>
        <source>Restart application to apply new icon color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5591"/>
        <source>The database can not be saved.

Please choose another location in the settings or all data will be lost.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2334"/>
        <location filename="../MainWindow.cpp" line="2384"/>
        <location filename="../MainWindow.cpp" line="2460"/>
        <location filename="../MainWindow.cpp" line="2591"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2335"/>
        <location filename="../MainWindow.cpp" line="2385"/>
        <location filename="../MainWindow.cpp" line="2461"/>
        <location filename="../MainWindow.cpp" line="2592"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2744"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2744"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2800"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1128"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1133"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2340"/>
        <location filename="../MainWindow.cpp" line="2390"/>
        <location filename="../MainWindow.cpp" line="2466"/>
        <location filename="../MainWindow.cpp" line="2597"/>
        <source>%1 (Age: %2, Height: %3cm, Weight: %4Kg, BMI: %5)</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2782"/>
        <location filename="../MainWindow.cpp" line="4015"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2786"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2791"/>
        <location filename="../MainWindow.cpp" line="4019"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3170"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3223"/>
        <location filename="../MainWindow.cpp" line="3227"/>
        <source>Export to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3223"/>
        <location filename="../MainWindow.cpp" line="3227"/>
        <source>%1 File (*.%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3263"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3269"/>
        <source>The database is empty, no records to export!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3620"/>
        <source>Morning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3620"/>
        <source>Afternoon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3659"/>
        <source>Week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3714"/>
        <source>Quarter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3743"/>
        <source>Half Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3761"/>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3949"/>
        <source>Really delete all records for user %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3964"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4089"/>
        <source>Really delete all records?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4100"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4406"/>
        <source>- UBPM Application
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4421"/>
        <source>- UBPM Plugins
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4565"/>
        <location filename="../MainWindow.cpp" line="4569"/>
        <location filename="../MainWindow.cpp" line="4573"/>
        <location filename="../MainWindow.h" line="163"/>
        <location filename="../MainWindow.h" line="167"/>
        <location filename="../MainWindow.h" line="171"/>
        <source>Minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4566"/>
        <location filename="../MainWindow.cpp" line="4570"/>
        <location filename="../MainWindow.cpp" line="4574"/>
        <location filename="../MainWindow.h" line="164"/>
        <location filename="../MainWindow.h" line="168"/>
        <location filename="../MainWindow.h" line="172"/>
        <source>Maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4639"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1956"/>
        <location filename="../MainWindow.cpp" line="1973"/>
        <location filename="../MainWindow.cpp" line="5473"/>
        <source>Delete record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5396"/>
        <source>Show Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5400"/>
        <source>Show Heartrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5436"/>
        <location filename="../MainWindow.cpp" line="5447"/>
        <source>Symbols and lines can&apos;t be disabled both!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5475"/>
        <source>Show record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1957"/>
        <location filename="../MainWindow.cpp" line="1980"/>
        <location filename="../MainWindow.cpp" line="5476"/>
        <source>Hide record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1975"/>
        <location filename="../MainWindow.cpp" line="5487"/>
        <source>Really delete selected record?</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="4518"/>
        <source>The following %n translation(s) for &quot;%1&quot; could not be loaded:

%2
Hide this warning on program startup?</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5369"/>
        <source>Scrolling has reached top/bottom, show next/previous period?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5392"/>
        <source>Dynamic Scaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5393"/>
        <source>Colored Stripes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5395"/>
        <source>Show Symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5398"/>
        <source>Colored Symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5508"/>
        <source>Show Median</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5509"/>
        <source>Show Values</source>
        <translation>Εμφάνιση τιμών</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5560"/>
        <source>Really quit program?</source>
        <translation>Πραγματικά εγκαταλείπετε το πρόγραμμα;</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Καθολικός διευθυντής πίεσης αίματος</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="13"/>
        <source>The application is already running and multiple instances are not allowed.

Switch to the running instance or close it and try again.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
