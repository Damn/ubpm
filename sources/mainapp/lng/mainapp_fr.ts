<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>DialogAbout</name>
    <message>
        <location filename="../DialogAbout.ui" line="14"/>
        <source>About UBPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="41"/>
        <source>Version</source>
        <translation type="unfinished">Version</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="53"/>
        <source>Universal Blood Pressure Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="91"/>
        <source>Qt Framework</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="129"/>
        <source>Operating System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="220"/>
        <source>Environment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="231"/>
        <source>Settings</source>
        <translation type="unfinished">Paramètres</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="269"/>
        <source>Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="307"/>
        <source>Cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="345"/>
        <source>Guides</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="383"/>
        <source>Languages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="421"/>
        <source>Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="459"/>
        <source>Themes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="490"/>
        <source>Translators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.cpp" line="14"/>
        <source>The program is provided as is with no warranty of any kind, including the warranty of design, merchantability and fitness for a particular purpose.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="14"/>
        <source>Data Analysis</source>
        <translation>Analyse des données</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="24"/>
        <source>Query</source>
        <translation>Requête</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="43"/>
        <location filename="../DialogAnalysis.cpp" line="209"/>
        <source>Results</source>
        <translation>Résultat</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="82"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="91"/>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="100"/>
        <source>Systolic</source>
        <translation>Systolique</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="109"/>
        <source>Diastolic</source>
        <translation>Distolique</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="118"/>
        <source>P.Pressure</source>
        <translation>Pression pulsée</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="127"/>
        <source>Heartrate</source>
        <translation>Fréquence cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="136"/>
        <source>Irregular</source>
        <translation>Irrégulier</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="145"/>
        <source>Movement</source>
        <translation>Mouvement</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="154"/>
        <source>Invisible</source>
        <translation>Invisible</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="163"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="193"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="51"/>
        <source>Could not create memory database!

%1</source>
        <translation>Impossible de créer une base de données mémoire&#xa0;!

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>No results for this query found!</source>
        <translation>Aucun résultat pour cette requête n&apos;a été trouvé&#xa0;!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Résultat</numerusform>
            <numerusform>Résultats</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Match(es)</source>
        <translation>
            <numerusform>Corresponde</numerusform>
            <numerusform>Correspondent</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Enregistrement</numerusform>
            <numerusform>Enregistrements</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>User %1</source>
        <translation>Utilisateur %1</translation>
    </message>
</context>
<context>
    <name>DialogDistribution</name>
    <message>
        <location filename="../DialogDistribution.ui" line="20"/>
        <source>Data Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="77"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="88"/>
        <source>Print</source>
        <translation type="unfinished">Imprimer</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="99"/>
        <source>Close</source>
        <translation type="unfinished">Fermer</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogDistribution.cpp" line="18"/>
        <source>Record(s)</source>
        <translation type="unfinished">
            <numerusform>Enregistrement</numerusform>
            <numerusform>Enregistrements</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="119"/>
        <source>Low</source>
        <translation type="unfinished">Basse</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="121"/>
        <source>Optimal</source>
        <translation type="unfinished">Optimale</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="123"/>
        <source>Normal</source>
        <translation type="unfinished">Normale</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="125"/>
        <source>High Normal</source>
        <translation type="unfinished">Haute normale</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="127"/>
        <source>Hyper 1</source>
        <translation type="unfinished">Hyper 1</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="129"/>
        <source>Hyper 2</source>
        <translation type="unfinished">Hyper 2</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="131"/>
        <source>Hyper 3</source>
        <translation type="unfinished">Hyper 3</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="148"/>
        <source>SYS</source>
        <translation type="unfinished">SYS</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="160"/>
        <source>DIA</source>
        <translation type="unfinished">DIA</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="171"/>
        <source>Blood Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="210"/>
        <source>Athlete</source>
        <translation type="unfinished">Athlète</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="212"/>
        <source>Excellent</source>
        <translation type="unfinished">Excellente</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="214"/>
        <source>Great</source>
        <translation type="unfinished">Superbe</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="216"/>
        <source>Good</source>
        <translation type="unfinished">Bonne</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="218"/>
        <source>Average</source>
        <translation type="unfinished">Moyenne</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="220"/>
        <source>Below Average</source>
        <translation type="unfinished">Sous la moyenne</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="222"/>
        <source>Poor</source>
        <translation type="unfinished">Pauvre</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="239"/>
        <source>BPM</source>
        <translation type="unfinished">BPM</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="265"/>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="346"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation type="unfinished">Crée avec UBPM pour
Windows / Linux / macOS</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="347"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation type="unfinished">Logiciel libre et gratuit
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="352"/>
        <source>%1 (Age: %2, Height: %3cm, Weight: %4Kg, BMI: %5)</source>
        <translation type="unfinished">%1 (Âge : %2, Taille : %3cm, Poids : %4Kg, BMI : %5)</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="352"/>
        <source>User %1</source>
        <translation type="unfinished">Utilisateur %1</translation>
    </message>
</context>
<context>
    <name>DialogDonation</name>
    <message>
        <location filename="../DialogDonation.ui" line="14"/>
        <source>Donation</source>
        <translation>Don</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="67"/>
        <source>Copy E-Mail and open Amazon</source>
        <translation>Copier le courriel et ouvrir Amazon</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="154"/>
        <source>Open PayPal (+)</source>
        <translation>Ouvrir PayPal (+)</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="171"/>
        <source>Open PayPal (-)</source>
        <translation>Ouvrir PayPal (-)</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="46"/>
        <source>If you have an Amazon account, you can send money by gift card.

Click the button below to open the Amazon website in your browser and

- sign in to your account
- select or enter the amount you would like to send
- select e-mail delivery
- paste the e-mail address as the recipient
- add a message
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation>Si vous avez un compte Amazon, vous pouvez envoyer de l&apos;argent par carte-cadeau.

Cliquez sur le bouton ci-dessous pour ouvrir le site web d&apos;Amazon dans votre navigateur et

- connectez-vous à votre compte
- sélectionnez ou saisissez le montant que vous souhaitez envoyer
- sélectionnez l&apos;envoi par courrier électronique
- collez l&apos;adresse électronique en tant que destinataire
- ajouter un message
- compléter le don

Vous pouvez également scanner le code QR avec votre appareil mobile.</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="124"/>
        <source>If you have a PayPal account, you can send money using the &quot;Friends and Family&quot; feature.

Open your PayPal app and

- search for @LazyT (Thomas Löwe)
- enter the amount you would like to send
- add a message
- complete the donation

You can also click the first button below to do this in your browser without using the app.

If you do not have a PayPal account but do have a credit/debit card click the second button below to go to the PayPal donation page and

- select onetime or repeating donation
- select or enter the amount you would like to send
- optionally check the box to cover the fees
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation>Si vous avez un compte PayPal, vous pouvez envoyer de l&apos;argent en utilisant la fonction « Amis et famille ».

Ouvrez votre application PayPal et

- recherchez @LazyT (Thomas Löwe)
- entrez le montant que vous souhaitez envoyer
- ajoutez un message
- compléter le don

Vous pouvez également cliquer sur le premier bouton ci-dessous pour effectuer cette opération dans votre navigateur sans utiliser l&apos;application.

Si vous n&apos;avez pas de compte PayPal mais que vous possédez une carte de crédit/débit, cliquez sur le deuxième bouton ci-dessous pour accéder à la page de don PayPal et

- sélectionnez un don unique ou répétitif
- sélectionnez ou entrez le montant que vous souhaitez envoyer
- cochez éventuellement la case pour couvrir les frais
- compléter le don

Vous pouvez également scanner le code QR avec votre appareil mobile.</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="230"/>
        <source>If you are in the Euro Payments Area, you can transfer money from your bank account.

Click the buttons below to copy the name/IBAN/BIC to the clipboard and

- sign in to your bank account
- paste the name/IBAN/BIC into the transfer form
- enter the amount you would like to send
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation>Si vous vous trouvez dans l&apos;espace de paiement en euros, vous pouvez transférer de l&apos;argent à partir de votre compte bancaire.

Cliquez sur les boutons ci-dessous pour copier le nom/IBAN/BIC dans le presse-papiers et

- vous connecter à votre compte bancaire
- coller le nom/IBAN/BIC dans le formulaire de transfert
- saisir le montant que vous souhaitez envoyer
- compléter le don

Vous pouvez également scanner le code QR avec votre appareil mobile.</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="251"/>
        <source>Copy Name</source>
        <translation>Copier le nom</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="268"/>
        <source>Copy IBAN</source>
        <translation>Copier l&apos;IBAN</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="285"/>
        <source>Copy BIC</source>
        <translation>Copier le BIC</translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Manuel utilisateur</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="61"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>Le Manuel Utilisateur %1 n&apos;a pas été trouvé, Regarder pour le moment de guide EN</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="53"/>
        <source>%1 guide not found!</source>
        <translation>%1 guide pas trouvé !</translation>
    </message>
</context>
<context>
    <name>DialogMigration</name>
    <message>
        <location filename="../DialogMigration.ui" line="14"/>
        <source>Data Migration</source>
        <translation>Migration des données</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="24"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="72"/>
        <source>Open Data Source</source>
        <translation>Ouvrir la source des données</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="97"/>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="217"/>
        <source>Date Format</source>
        <translation>Format de la date</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="267"/>
        <source>Irregular Detection</source>
        <translation>Détection d&apos;irrégularités</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="167"/>
        <source>Startline</source>
        <translation>Ligne de départ</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="183"/>
        <source>Elements per Line</source>
        <translation>Éléments par ligne</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="111"/>
        <source>Delimiter</source>
        <translation>Délimiteur</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="303"/>
        <source>Time Format</source>
        <translation>Format de l’heure</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="199"/>
        <source>Movement Detection</source>
        <translation>Détection des mouvements</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="239"/>
        <source>Date Language</source>
        <translation>Langue de la date</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="344"/>
        <source>Positions</source>
        <translation>Positions</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="488"/>
        <source>Irregular</source>
        <translation>Irrégulier</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="504"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="452"/>
        <source>Systolic</source>
        <translation>Systolique</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="410"/>
        <source>Heartrate</source>
        <translation>Fréquence cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="394"/>
        <source>Movement</source>
        <translation>Mouvement</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="329"/>
        <source>Ignore Comment</source>
        <translation>Ignorer le commentaire</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="368"/>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="436"/>
        <source>Diastolic</source>
        <translation>Diastolique</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="550"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="567"/>
        <source>Result</source>
        <translation>Résultat</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="786"/>
        <source>Migrate User 1</source>
        <translation>Migrer l&apos;Utilisateur 1</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="815"/>
        <source>Migrate User 2</source>
        <translation>Migrer l&apos;Utilisateur 2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="841"/>
        <source>Copy to Custom</source>
        <translation>Copier dans Personnalisé</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="861"/>
        <source>Select predefined settings</source>
        <translation>Sélectionner les réglages prédéfinis</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="895"/>
        <source>Custom</source>
        <translation>Personnalisé</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="906"/>
        <source>Test Data Migration</source>
        <translation>Test de la migration des données</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="920"/>
        <source>Start Data Migration</source>
        <translation>Démarrer la migration des données</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="251"/>
        <source>Choose Data Source for Migration</source>
        <translation>Choisir la source de données pour la migration</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="251"/>
        <source>CSV File (*.csv)</source>
        <translation>Fichier CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="304"/>
        <source>%L1 Bytes</source>
        <translation>%L1 octets</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="305"/>
        <source>%1 Lines</source>
        <translation>%1 lignes</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="319"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Impossible d&apos;ouvrir «&#xa0;%1&#xa0;»&#xa0;!

Raison&#xa0;: %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="341"/>
        <source>Startline can&apos;t be empty.</source>
        <translation>La ligne de début ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="347"/>
        <source>Startline must be smaller than the number of lines.</source>
        <translation>La ligne de début doit être plus petite que le nombre de lignes.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="355"/>
        <source>Line %1 = %2</source>
        <translation>Ligne %1 = %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="360"/>
        <source>Delimiter can&apos;t be empty.</source>
        <translation>Le délimiteur ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="367"/>
        <source>Startline doesn&apos;t contain &quot;%1&quot; delimiter.</source>
        <translation>La ligne de début ne contient pas le délimiteur «&#xa0;%1&#xa0;».</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="378"/>
        <source>Elements can&apos;t be empty.</source>
        <translation>Les éléments ne peuvent pas être vides.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="384"/>
        <source>Elements can&apos;t be smaller than 4.</source>
        <translation>Les éléments ne peuvent être inférieurs à 4.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="390"/>
        <source>Elements doesn&apos;t match (found %1).</source>
        <translation>Les éléments ne correspondent pas (trouvé %1).</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="397"/>
        <source>Date format can&apos;t be empty.</source>
        <translation>Le format de la date ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="403"/>
        <source>Single date format must also contain a time format.</source>
        <translation>Un format de date unique doit également contenir un format d&apos;heure.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="410"/>
        <source>Date position can&apos;t be empty.</source>
        <translation>La position de la date ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="416"/>
        <source>Date position must be smaller than elements.</source>
        <translation>La position de la date doit être plus petite que les éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="423"/>
        <source>Time position requires also a time parameter.</source>
        <translation>La position temporelle nécessite également un paramètre temporel.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="429"/>
        <source>Time format requires also a time position.</source>
        <translation>Le format horaire nécessite également une position horaire.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="435"/>
        <source>Time position must be smaller than elements.</source>
        <translation>La position temporelle doit être inférieure aux éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="442"/>
        <source>Systolic position can&apos;t be empty.</source>
        <translation>La position systolique ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="448"/>
        <source>Systolic position must be smaller than elements.</source>
        <translation>La position systolique doit être inférieure aux éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="455"/>
        <source>Diastolic position can&apos;t be empty.</source>
        <translation>La position diastolique ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="461"/>
        <source>Diastolic position must be smaller than elements.</source>
        <translation>La position diastolique doit être inférieure aux éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="468"/>
        <source>Heartrate position can&apos;t be empty.</source>
        <translation>La position de la fréquence cardiaque ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="474"/>
        <source>Heartrate position must be smaller than elements.</source>
        <translation>La position de la fréquence cardiaque doit être inférieure aux éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="481"/>
        <source>Irregular position must be smaller than elements.</source>
        <translation>La position irrégulière doit être plus petite que les éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="487"/>
        <source>Irregular position requires also an irregular parameter.</source>
        <translation>Une position irrégulière nécessite également un paramètre irrégulier.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="494"/>
        <source>Movement position must be smaller than elements.</source>
        <translation>La position du mouvement doit être plus petite que les éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="500"/>
        <source>Movement position requires also a movement parameter.</source>
        <translation>La position du mouvement nécessite également un paramètre de mouvement.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="507"/>
        <source>Comment position must be smaller than elements.</source>
        <translation>La position du commentaire doit être plus petite que celle des éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="523"/>
        <source>Positions can&apos;t exist twice.</source>
        <translation>Les positions ne peuvent pas exister deux fois.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="539"/>
        <source>Date format is invalid.</source>
        <translation>Le format de la date est invalide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="546"/>
        <source>Time format is invalid.</source>
        <translation>Le format de l&apos;heure est invalide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="556"/>
        <source>Date/Time format is invalid.</source>
        <translation>Le format de la date et de l&apos;heure est invalide.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="747"/>
        <source>Successfully migrated %n record(s) for user %1.</source>
        <translation>
            <numerusform>La migration de %n enregistrement pour l&apos;utilisateur %1 a réussi.</numerusform>
            <numerusform>La migration de %n enregistrements pour l&apos;utilisateur %1 a réussi.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="751"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>Saut de %n enregistrement invalide&#xa0;!</numerusform>
            <numerusform>Saut de %n enregistrements invalides&#xa0;!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="756"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>Sauter %n enregistrement en double&#xa0;!</numerusform>
            <numerusform>Sauter %n enregistrements en double&#xa0;!</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Enregistrement Manuel</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Enregistrement de données</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="211"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Ajouter un enregistrement pour %1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Sélectionner la date et la heure</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation>Entrer SYS</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="148"/>
        <source>Enter DIA</source>
        <translation>Entrer DIA</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="167"/>
        <source>Enter BPM</source>
        <translation>Entrer BPM</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation>Rythme cardiaque irrégulier</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="186"/>
        <source>Movement</source>
        <translation>Mouvement</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="239"/>
        <source>Enter Optional Comment</source>
        <translation>Entrez un commentaire facultatif</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="269"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Afficher un message pour une création / suppression / modification d&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="287"/>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="304"/>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="324"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Utilisateur 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Utilisateur 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="34"/>
        <location filename="../DialogRecord.cpp" line="187"/>
        <source>Modify</source>
        <translation>Modifier</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="132"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>L&apos;Enregistrement n&apos;a pas pu être supprimé

Aucune Entrée pour cette date et heure n&apos;existe</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="138"/>
        <source>Data record successfully deleted.</source>
        <translation>L&apos;enregistrement a été effacé avec succès</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="150"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>Veuillez d&apos;abord entrer une valeur valide pour &quot;SYS&quot; !</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="160"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>Veuillez d&apos;abord entrer une valeur valide pour &quot;DIA&quot; !</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="169"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>Veuillez d&apos;abord entrer une valeur valide pour &quot;BPM&quot; !</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="191"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>L&apos;Enregistrement n&apos;a pas pu être modifié.

Aucune entrée pour cette date et cette heure existe.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="197"/>
        <source>Data Record successfully modified.</source>
        <translation>Les Données de l&apos;Enregistrement ont été modifiées avec Succès.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="206"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>Les Données de l&apos;Enregistrement n&apos;ont pas pu être crées !

Une entrée avec la Date et l&apos;Heure existe déjà.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="212"/>
        <source>Data Record successfully created.</source>
        <translation>L&apos;Enregistrement de la Données est crée avec Succès.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.cpp" line="167"/>
        <source>Choose Database Location</source>
        <translation>Choisir l&apos;Emplacement de la Base De Données</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="177"/>
        <source>Choose Database Backup Location</source>
        <translation>Choisir l&apos;emplacement de la sauvegarde de la base de données</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="211"/>
        <source>Could not start pdf viewer!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="216"/>
        <source>Could not open manual &quot;%1&quot;!

%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="339"/>
        <source>The selected database already exists.

Please choose the preferred action.</source>
        <translation>La base de données sélectionnée existe déjà.

Veuillez choisir l&apos;action préférée.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="341"/>
        <source>Overwrite</source>
        <translation>Écraser</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="342"/>
        <source>Merge</source>
        <translation>Fusionner</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="343"/>
        <source>Swap</source>
        <translation>Permuter</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="345"/>
        <source>Overwrite:

Overwrites the selected database with the current database.

Merge:

Merges the selected database into the current database.

Swap:

Deletes the current database and use the selected database.</source>
        <translation>Écraser :

Écrase la base de données sélectionnée avec la base de données actuelle.

Fusionner :

Fusionne la base de données sélectionnée avec la base de données actuelle.

Permuter :

Supprime la base de données actuelle et utilise la base de données sélectionnée.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="353"/>
        <source>The current database is empty.

The selected database will be erased on exit, if no data will be added.</source>
        <translation>La base de données actuelle est vide.

La base de données sélectionnée sera effacée à la sortie, si aucune donnée n&apos;est ajoutée.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="383"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>Le cryptage SQL ne peut être activé sans mot de passe et sera désactivé !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="390"/>
        <source>The database backup should be located on a different hard disk, partition or directory.</source>
        <translation>La sauvegarde de la base de données doit être située sur un disque dur, une partition ou un répertoire différent.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="395"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Veuillez entrer des valeurs valides pour les informations supplémentaires de l&apos;utilisateur 1 !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="402"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Veuillez entrer des valeurs valides pour les informations supplémentaires de l&apos;utilisateur 2 !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="409"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>L&apos;age saisi ne correspond pas à la tranche d&apos;age sélectionné !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="416"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>L&apos;age saisi ne correspond pas à la tranche d&apos;age sélectionné pour l&apos;utilisateur 2 !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="424"/>
        <source>Please enable symbols or lines for chart!</source>
        <translation>Veuillez activer les symboles ou les lignes pour les graphiques !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="431"/>
        <source>Please enter a valid e-mail address!</source>
        <translation>Merci d&apos;entrer un adresse électronique valide&#xa0;!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="438"/>
        <source>Please enter a e-mail subject!</source>
        <translation>Merci d&apos;entrer un objet à votre courriel&#xa0;!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="445"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation>un message doit contenir $CHART, $TABLE et/ou $STATS !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="556"/>
        <source>User 1</source>
        <translation>Utilisateur 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="570"/>
        <source>User 2</source>
        <translation>Utilisateur 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="683"/>
        <source>Blood Pressure Report</source>
        <translation>Rapport sur la tension artérielle</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="684"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Cher Dr NomduPraticien,

veuillez trouver ci-joint les données de ma tension artérielle pour ce mois-ci.

Cordialement,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="726"/>
        <source>Abort setup and discard all changes?</source>
        <translation>Abandonner la configuration et abandonner toutes les modifications ?</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>Base de Données</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="52"/>
        <source>Location</source>
        <translation>Emplacement</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="58"/>
        <source>Current Location</source>
        <translation>Emplacement Courant</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="74"/>
        <source>Change Location</source>
        <translation>Changer Emplacement</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="100"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Crypter avec SQLCipher</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="103"/>
        <source>Encryption</source>
        <translation>Crypter</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="115"/>
        <source>Password</source>
        <translation>Mot de Passe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="128"/>
        <source>Show Password</source>
        <translation>Afficher Mot de Passe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="282"/>
        <source>User</source>
        <translation>Utilisateur</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="317"/>
        <location filename="../DialogSettings.ui" line="580"/>
        <source>Male</source>
        <translation>Homme</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="346"/>
        <location filename="../DialogSettings.ui" line="609"/>
        <source>Female</source>
        <translation>Femme</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="442"/>
        <location filename="../DialogSettings.ui" line="714"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="311"/>
        <location filename="../DialogSettings.ui" line="574"/>
        <source>Mandatory Information</source>
        <translation>Informations Obligatoires</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="372"/>
        <location filename="../DialogSettings.ui" line="644"/>
        <source>Age Group</source>
        <translation>Tranche d&apos;Age</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="458"/>
        <location filename="../DialogSettings.ui" line="730"/>
        <source>Additional Information</source>
        <translation>Informations Complémentaires</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="518"/>
        <location filename="../DialogSettings.ui" line="790"/>
        <source>Height</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="547"/>
        <location filename="../DialogSettings.ui" line="819"/>
        <source>Weight</source>
        <translation>Poids</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="844"/>
        <source>Device</source>
        <translation>Appareil</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="850"/>
        <location filename="../DialogSettings.cpp" line="70"/>
        <source>Import Plugins [ %1 ]</source>
        <translation>Importer des greffons [ %1 ]</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="880"/>
        <source>Show Device Image</source>
        <translation>Afficher l&apos;image de l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="903"/>
        <source>Show Device Manual</source>
        <translation>Afficher le manuel de l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="948"/>
        <source>Open Website</source>
        <translation>Aller au site Web</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="961"/>
        <source>Maintainer</source>
        <translation>Mainteneur</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="989"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="996"/>
        <source>Send E-Mail</source>
        <translation>Envoyer un courriel</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1009"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1016"/>
        <source>Producer</source>
        <translation>Fabricant</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1033"/>
        <source>Chart</source>
        <translation>Graphique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1160"/>
        <source>X-Axis Range</source>
        <translation>Plage de l&apos;axe X</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1166"/>
        <source>Dynamic Scaling</source>
        <translation>Échelle dynamique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1147"/>
        <source>Healthy Ranges</source>
        <translation>Plages saines</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1064"/>
        <source>Symbols</source>
        <translation>Symboles</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1141"/>
        <source>Colored Areas</source>
        <translation>Zones Colorées</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="865"/>
        <source>Please choose Device Plugin…</source>
        <translation>Veuillez choisir le greffon pour l&apos;appareil…</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1288"/>
        <location filename="../DialogSettings.ui" line="1563"/>
        <source>Systolic</source>
        <translation>Systolique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1376"/>
        <location filename="../DialogSettings.ui" line="1651"/>
        <source>Diastolic</source>
        <translation>Diastolique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1464"/>
        <location filename="../DialogSettings.ui" line="1739"/>
        <source>Heartrate</source>
        <translation>Fréquence Cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1836"/>
        <source>Table</source>
        <translation>Tableau</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1921"/>
        <location filename="../DialogSettings.ui" line="2104"/>
        <source>Systolic Warnlevel</source>
        <translation>Niveau d&apos;Alerte Systolique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1867"/>
        <location filename="../DialogSettings.ui" line="2164"/>
        <source>Diastolic Warnlevel</source>
        <translation>Niveau d&apos;Alerte Diastolique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="153"/>
        <source>Automatic Backup</source>
        <translation>Sauvegarde automatique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="156"/>
        <source>Backup</source>
        <translation>Sauvegarde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="173"/>
        <source>Current Backup Location</source>
        <translation>Emplacement actuel de la sauvegarde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="186"/>
        <source>Change Backup Location</source>
        <translation>Modifier l&apos;emplacement de la sauvegarde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="213"/>
        <source>Backup Mode</source>
        <translation>Mode de sauvegarde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="223"/>
        <source>Daily</source>
        <translation>Quotidien</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="232"/>
        <source>Weekly</source>
        <translation>Hebdomadaire</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="241"/>
        <source>Monthly</source>
        <translation>Mensuel</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="253"/>
        <source>Keep Copies</source>
        <translation>Conserver des copies</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="486"/>
        <location filename="../DialogSettings.ui" line="758"/>
        <source>Birthday</source>
        <translation>Anniversaire</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1073"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1097"/>
        <location filename="../DialogSettings.ui" line="1125"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1179"/>
        <source>Lines</source>
        <translation>Lignes</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1188"/>
        <location filename="../DialogSettings.ui" line="1216"/>
        <source>Width</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1245"/>
        <source>Show Heartrate</source>
        <translation>Afficher la Fréquence Cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1251"/>
        <source>Show Heartrate in Chart View</source>
        <translation>Afficher La Fréquence Cardiaque en Grahpique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1261"/>
        <source>Print Heartrate</source>
        <translation>Imprimer Fréquence Cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1267"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation>Imprimer la Fréquence Cardiaque sur une feuille séparée</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1981"/>
        <location filename="../DialogSettings.ui" line="2218"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Avertissement de la Pression Cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2035"/>
        <location filename="../DialogSettings.ui" line="2272"/>
        <source>Heartrate Warnlevel</source>
        <translation>Avertissement de la Fréquence Cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2337"/>
        <source>Statistic</source>
        <translation>Statistique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2343"/>
        <source>Bar Type</source>
        <translation>Type de Barre</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2349"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Afficher les Barres Médianes au lieu des Barres Moyennes</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2359"/>
        <source>Legend Type</source>
        <translation>Type de Légende</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2365"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Afficher les Valeurs en tant que Légende à la place des Descriptions</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2380"/>
        <source>E-Mail</source>
        <translation>Courriel</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2388"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2407"/>
        <source>Subject</source>
        <translation>Sujet</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2425"/>
        <source>Message</source>
        <translation>Message</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2446"/>
        <source>Plugin</source>
        <translation>Greffon</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2452"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2458"/>
        <source>Log Device Communication to File</source>
        <translation>Enregistrer la communication du dispositif dans un fichier</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2465"/>
        <source>Import Measurements automatically</source>
        <translation>Importer automatiquement les mesures</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2475"/>
        <source>Bluetooth</source>
        <translation>Bluetooth</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2481"/>
        <source>Discover Device automatically</source>
        <translation>Découvrir le dispositif automatiquement</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2488"/>
        <source>Connect Device automatically</source>
        <translation>Connecter le dispositif automatiquement</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2503"/>
        <source>Update</source>
        <translation>Mise à Jour</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2509"/>
        <source>Autostart</source>
        <translation>Démmarage Automatique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2515"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Vérifier si une Mise à Jour au Démarrage du Programme</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2528"/>
        <source>Notification</source>
        <translation>Notification</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2534"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Toujours montrer le Résultat après une vérification de Mise à Jour</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2550"/>
        <source>Save</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2567"/>
        <source>Reset</source>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2584"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Mise à jour en Ligne</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Version Disponible</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Taille du Fichier de Mise à Jour</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Version Installée</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="178"/>
        <location filename="../DialogUpdate.ui" line="199"/>
        <source>Download</source>
        <translation>Télécharger</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="219"/>
        <source>Ignore</source>
        <translation>Ignorer</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="183"/>
        <source>No new version found.</source>
        <translation>Pas de nouvelle version trouvée.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="42"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform>!!! AVERTISSEMENT SSL - LIRE ATTENTIVEMENT !!!

Problème de connexion réseau :

%1
Voulez-vous quand même continuer ?</numerusform>
            <numerusform>!!! AVERTISSEMENT SSL - LIRE ATTENTIVEMENT !!!

Problèmes de connexion réseau :

%1
Voulez-vous quand même continuer ?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="73"/>
        <source>Downloading update failed!

%1</source>
        <translation>Le Téléchargement de la Mise à Jour a échoué !

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="73"/>
        <source>Checking update failed!

%1</source>
        <translation>La Vérification de la Mise à Jour a échouée !

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="156"/>
        <source>Unexpected response from update server!</source>
        <translation>Réponse inattendu du serveur de mise à jour !</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="173"/>
        <source>*%1 not found</source>
        <translation>*%1 pas trouvé</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="203"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation>La mise à jour n&apos;a pas la taille attendue !

%L1 : %L2

Réessayer de Télécharger …</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="207"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>La mise à jour a été enregistrée dans %1.

Lancer la nouvelle version maintenant ?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="220"/>
        <source>Could not start new version!</source>
        <translation>Impossible de lancer la nouvelle version !</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="247"/>
        <source>The program was installed by Flatpak.

Please update using the Flatpak internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="253"/>
        <source>The program was installed by Snap.

Please update using the Snap internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="259"/>
        <source>The program was installed from distribution.

Please update using the operating systems internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="265"/>
        <source>The program was installed from source code.

Please update the sources, rebuild and reinstall manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="291"/>
        <source>Really abort download?</source>
        <translation>Interrompre vraiment le téléchargement ?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="227"/>
        <source>Could not save update to %1!

%2</source>
        <translation>Impossible d&apos;enregistrer la mise à jour dans %1 !

%2</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="14"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Gestionnaire Universel de la Pression Artérielle</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="30"/>
        <source>Show Previous Period</source>
        <translation>Montrer Période Précédente</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="104"/>
        <source>Show Next Period</source>
        <translation>Montrer Période Suivante</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>Montrer Graphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Montrer Tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="159"/>
        <location filename="../MainWindow.cpp" line="4554"/>
        <source>Systolic</source>
        <translation>Systolique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="160"/>
        <location filename="../MainWindow.cpp" line="4555"/>
        <source>Diastolic</source>
        <translation>Diastolique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>Pression Pulsée</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Irrégulier</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation>Mouvement</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation>Invisible</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation>Vue Statistique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation>¼ Heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation>½ Heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation>6 Heures</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation>12 Heures</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation>Jour</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>Weekly</source>
        <translation>Hebdomadaire</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Monthly</source>
        <translation>Mensuel</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Quarterly</source>
        <translation>Trimestrielle</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>½ Yearly</source>
        <translation>Semestrielle</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>Yearly</source>
        <translation>Annuelle</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="793"/>
        <source>Last 7 Days</source>
        <translation>Les 7 Derniers Jours</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="828"/>
        <source>Last 14 Days</source>
        <translation>Les 14 Derniers Jours</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="860"/>
        <source>Last 21 Days</source>
        <translation>Les 21 Derniers Jours</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="892"/>
        <source>Last 28 Days</source>
        <translation>Les 28 Derniers Jours</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="924"/>
        <source>Last 3 Months</source>
        <translation>Les 3 Derniers Mois</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="956"/>
        <source>Last 6 Months</source>
        <translation>Les 6 Derniers Mois</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="988"/>
        <source>Last 9 Months</source>
        <translation>Les 9 Derniers Mois</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1020"/>
        <source>Last 12 Months</source>
        <translation>Les 12 Derniers Mois</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1052"/>
        <source>All Records</source>
        <translation>Tous les Enregistrements</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1099"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1103"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1125"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1141"/>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1145"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1154"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1163"/>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1172"/>
        <source>Charts</source>
        <translation>Graphiques</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1197"/>
        <source>Database</source>
        <translation>Basse de Donnée</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1201"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1213"/>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1228"/>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1317"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1320"/>
        <location filename="../MainWindow.ui" line="1323"/>
        <source>Quit Program</source>
        <translation>Quitter le programme</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1335"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1338"/>
        <location filename="../MainWindow.ui" line="1341"/>
        <source>About Program</source>
        <translation>À propos du programme</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1350"/>
        <source>Guide</source>
        <translation>Guide</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1353"/>
        <location filename="../MainWindow.ui" line="1356"/>
        <source>Show Guide</source>
        <translation>Afficher le guide</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1365"/>
        <source>Update</source>
        <translation>Mise à jour</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1368"/>
        <location filename="../MainWindow.ui" line="1371"/>
        <source>Check Update</source>
        <translation>Vérifier les mises à jour</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1530"/>
        <source>To PDF</source>
        <translation>En PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1533"/>
        <location filename="../MainWindow.ui" line="1536"/>
        <source>Export To PDF</source>
        <translation>Exporter comme PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1545"/>
        <source>Migration</source>
        <translation>Migration</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1548"/>
        <location filename="../MainWindow.ui" line="1551"/>
        <source>Migrate from Vendor</source>
        <translation>Migrer d&apos;un fournisseur</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1795"/>
        <source>Translation</source>
        <translation>Traduction</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1798"/>
        <location filename="../MainWindow.ui" line="1801"/>
        <source>Contribute Translation</source>
        <translation>Contribuer à la traduction</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1810"/>
        <source>E-Mail</source>
        <translation>Courriel</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1813"/>
        <location filename="../MainWindow.ui" line="1816"/>
        <source>Send E-Mail</source>
        <translation>Envoyer le courriel</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1828"/>
        <source>Icons</source>
        <translation>Icônes</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1831"/>
        <location filename="../MainWindow.ui" line="1834"/>
        <source>Change Icon Color</source>
        <translation>Changer la couleur de l&apos;icône</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1845"/>
        <source>System</source>
        <translation>Système</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1848"/>
        <location filename="../MainWindow.ui" line="1851"/>
        <source>System Colors</source>
        <translation>Couleurs du système</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1859"/>
        <source>Light</source>
        <translation>Clair</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1862"/>
        <location filename="../MainWindow.ui" line="1865"/>
        <source>Light Colors</source>
        <translation>Couleurs claires</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1873"/>
        <source>Dark</source>
        <translation>Sombre</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1876"/>
        <location filename="../MainWindow.ui" line="1879"/>
        <source>Dark Colors</source>
        <translation>Couleurs sombres</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1888"/>
        <source>Donation</source>
        <translation>Faire un don</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1903"/>
        <source>Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1906"/>
        <location filename="../MainWindow.ui" line="1909"/>
        <source>Show Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="161"/>
        <location filename="../MainWindow.cpp" line="4556"/>
        <source>Heartrate</source>
        <translation>Fréquence cardiaque</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1891"/>
        <location filename="../MainWindow.ui" line="1894"/>
        <source>Make Donation</source>
        <translation>Faire un don</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1383"/>
        <source>Bugreport</source>
        <translation>Rapport d&apos;erreur</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1386"/>
        <location filename="../MainWindow.ui" line="1389"/>
        <source>Send Bugreport</source>
        <translation>Envoyer un rapport d&apos;erreur</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1398"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1401"/>
        <location filename="../MainWindow.ui" line="1404"/>
        <source>Change Settings</source>
        <translation>Charger paramètres</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1419"/>
        <source>From Device</source>
        <translation>Depuis l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1422"/>
        <location filename="../MainWindow.ui" line="1425"/>
        <source>Import From Device</source>
        <translation>Importer depuis l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1437"/>
        <source>From File</source>
        <translation>Depuis un fichier</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1440"/>
        <location filename="../MainWindow.ui" line="1443"/>
        <source>Import From File</source>
        <translation>Importer depuis un fichier</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1452"/>
        <source>From Input</source>
        <translation>Depuis l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1455"/>
        <location filename="../MainWindow.ui" line="1458"/>
        <source>Import From Input</source>
        <translation>Importer depuis l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1467"/>
        <source>To CSV</source>
        <translation>En CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1470"/>
        <location filename="../MainWindow.ui" line="1473"/>
        <source>Export To CSV</source>
        <translation>Exporter en CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1485"/>
        <source>To XML</source>
        <translation>En XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1488"/>
        <location filename="../MainWindow.ui" line="1491"/>
        <source>Export To XML</source>
        <translation>Exporter en XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1500"/>
        <source>To JSON</source>
        <translation>En JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1503"/>
        <location filename="../MainWindow.ui" line="1506"/>
        <source>Export To JSON</source>
        <translation>Exporter en JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1515"/>
        <source>To SQL</source>
        <translation>En SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1518"/>
        <location filename="../MainWindow.ui" line="1521"/>
        <source>Export To SQL</source>
        <translation>Exporter en SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1563"/>
        <source>Print Chart</source>
        <translation>Imprimer le gaphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1566"/>
        <location filename="../MainWindow.ui" line="1569"/>
        <source>Print Chart View</source>
        <translation>Aperçu de l&apos;impression graphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1578"/>
        <source>Print Table</source>
        <translation>Imprimer le tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1581"/>
        <location filename="../MainWindow.ui" line="1584"/>
        <source>Print Table View</source>
        <translation>Aperçu de l&apos;impression tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1593"/>
        <source>Print Statistic</source>
        <translation>Imprimer les statistiques</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1596"/>
        <location filename="../MainWindow.ui" line="1599"/>
        <source>Print Statistic View</source>
        <translation>Aperçu de l&apos;impression statistiques</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1608"/>
        <source>Preview Chart</source>
        <translation>Aperçu du graphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1611"/>
        <location filename="../MainWindow.ui" line="1614"/>
        <source>Preview Chart View</source>
        <translation>Aperçu de la vue graphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1623"/>
        <source>Preview Table</source>
        <translation>Aperçu tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1626"/>
        <location filename="../MainWindow.ui" line="1629"/>
        <source>Preview Table View</source>
        <translation>Aperçu de la vue tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1638"/>
        <source>Preview Statistic</source>
        <translation>Aperçu des statistiques</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1641"/>
        <location filename="../MainWindow.ui" line="1644"/>
        <source>Preview Statistic View</source>
        <translation>Aperçu de la vue statistiques</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1653"/>
        <location filename="../MainWindow.ui" line="1656"/>
        <location filename="../MainWindow.ui" line="1659"/>
        <source>Clear All</source>
        <translation>Effacer tout</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1671"/>
        <location filename="../MainWindow.ui" line="1674"/>
        <location filename="../MainWindow.ui" line="1677"/>
        <source>Clear User 1</source>
        <translation>Effacer Utilisateur 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1686"/>
        <location filename="../MainWindow.ui" line="1689"/>
        <location filename="../MainWindow.ui" line="1692"/>
        <source>Clear User 2</source>
        <translation>Effacer Utilisateur 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1752"/>
        <source>Analysis</source>
        <translation>Analyser</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1755"/>
        <location filename="../MainWindow.ui" line="1758"/>
        <source>Analyze Records</source>
        <translation>Analyser les enregistrements</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1777"/>
        <location filename="../MainWindow.ui" line="1780"/>
        <location filename="../MainWindow.ui" line="1783"/>
        <source>Time Mode</source>
        <translation>Mode Heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="108"/>
        <location filename="../MainWindow.cpp" line="109"/>
        <location filename="../MainWindow.cpp" line="4524"/>
        <location filename="../MainWindow.cpp" line="4525"/>
        <source>Records For Selected User</source>
        <translation>Enregistrements pour l&apos;utilisateur sélectionné</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="116"/>
        <location filename="../MainWindow.cpp" line="117"/>
        <location filename="../MainWindow.cpp" line="4527"/>
        <location filename="../MainWindow.cpp" line="4528"/>
        <source>Select Date &amp; Time</source>
        <translation>Sélectionner la date et l&apos;heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="169"/>
        <location filename="../MainWindow.cpp" line="4558"/>
        <source>Systolic - Value Range</source>
        <translation>Systolique - Plage de valeurs</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="170"/>
        <location filename="../MainWindow.cpp" line="4559"/>
        <source>Diastolic - Value Range</source>
        <translation>Diastolique - Plage de valeurs</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="173"/>
        <location filename="../MainWindow.cpp" line="4561"/>
        <source>Systolic - Target Area</source>
        <translation>Systolique - Zone cible</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="174"/>
        <location filename="../MainWindow.cpp" line="4562"/>
        <source>Diastolic - Target Area</source>
        <translation>Diastolique - Zone cible</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1617"/>
        <source>Athlete</source>
        <translation>Athlète</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1618"/>
        <source>Excellent</source>
        <translation>Excellente</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1618"/>
        <source>Optimal</source>
        <translation>Optimale</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1619"/>
        <source>Great</source>
        <translation>Superbe</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1620"/>
        <source>Good</source>
        <translation>Bonne</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1619"/>
        <source>Normal</source>
        <translation>Normale</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1713"/>
        <location filename="../MainWindow.ui" line="1716"/>
        <location filename="../MainWindow.ui" line="1737"/>
        <location filename="../MainWindow.ui" line="1740"/>
        <location filename="../MainWindow.cpp" line="102"/>
        <location filename="../MainWindow.cpp" line="103"/>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="4256"/>
        <location filename="../MainWindow.cpp" line="4257"/>
        <location filename="../MainWindow.cpp" line="4258"/>
        <location filename="../MainWindow.cpp" line="4259"/>
        <location filename="../MainWindow.cpp" line="4588"/>
        <location filename="../MainWindow.cpp" line="4589"/>
        <location filename="../MainWindow.cpp" line="4590"/>
        <location filename="../MainWindow.cpp" line="4591"/>
        <source>Switch To %1</source>
        <translation>Basculer vers %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="102"/>
        <location filename="../MainWindow.cpp" line="103"/>
        <location filename="../MainWindow.cpp" line="587"/>
        <location filename="../MainWindow.cpp" line="4256"/>
        <location filename="../MainWindow.cpp" line="4257"/>
        <location filename="../MainWindow.cpp" line="4588"/>
        <location filename="../MainWindow.cpp" line="4589"/>
        <source>User 1</source>
        <translation>Utilisateur 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="597"/>
        <location filename="../MainWindow.cpp" line="4258"/>
        <location filename="../MainWindow.cpp" line="4259"/>
        <location filename="../MainWindow.cpp" line="4590"/>
        <location filename="../MainWindow.cpp" line="4591"/>
        <source>User 2</source>
        <translation>Utilisateur 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="171"/>
        <location filename="../MainWindow.cpp" line="4560"/>
        <source>Heartrate - Value Range</source>
        <translation>Fréquence cardiaque - Plage de valeurs</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="175"/>
        <location filename="../MainWindow.cpp" line="4563"/>
        <source>Heartrate - Target Area</source>
        <translation>Fréquence cardiaque - Zone cible</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="341"/>
        <source>No active device plugin, auto import aborted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="448"/>
        <source>Could not create backup directory.

Please check backup location setting.</source>
        <translation>Impossible de créer le répertoire de sauvegarde.

Veuillez vérifier les paramètres de l&apos;emplacement de sauvegarde.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="477"/>
        <location filename="../MainWindow.cpp" line="500"/>
        <source>Could not backup database:

%1</source>
        <translation>Impossible de sauvegarder la base de données&#xa0;:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="490"/>
        <source>Could not delete outdated backup:

%1</source>
        <translation>Impossible de supprimer une sauvegarde périmée&#xa0;:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="517"/>
        <source>Could not register icons.</source>
        <translation>Impossible d&apos;enregistrer les icônes.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="650"/>
        <source>Blood Pressure Report</source>
        <translation>Rapport pression artérielle</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1129"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1134"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1617"/>
        <source>Low</source>
        <translation>Basse</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1620"/>
        <source>High Normal</source>
        <translation>Haute normale</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1621"/>
        <location filename="../MainWindow.cpp" line="2088"/>
        <location filename="../MainWindow.cpp" line="4567"/>
        <location filename="../MainWindow.cpp" line="4571"/>
        <location filename="../MainWindow.cpp" line="4575"/>
        <location filename="../MainWindow.h" line="165"/>
        <location filename="../MainWindow.h" line="169"/>
        <location filename="../MainWindow.h" line="173"/>
        <source>Average</source>
        <translation>Moyenne</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1621"/>
        <source>Hyper 1</source>
        <translation>Hyper 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1622"/>
        <source>Below Average</source>
        <translation>Sous la moyenne</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1622"/>
        <source>Hyper 2</source>
        <translation>Hyper 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1623"/>
        <source>Poor</source>
        <translation>Pauvre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1623"/>
        <source>Hyper 3</source>
        <translation>Hyper 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1768"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>l&apos;Analyse du plugin d&apos;importation «&#xa0;%1&#xa0;» a échoué&#xa0;!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1781"/>
        <location filename="../MainWindow.cpp" line="1803"/>
        <location filename="../MainWindow.cpp" line="4534"/>
        <source>Switch Language to %1</source>
        <translation>Changer la langue en %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1828"/>
        <location filename="../MainWindow.cpp" line="1843"/>
        <location filename="../MainWindow.cpp" line="4542"/>
        <source>Switch Theme to %1</source>
        <translation>Changer le thème en %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1868"/>
        <location filename="../MainWindow.cpp" line="1888"/>
        <location filename="../MainWindow.cpp" line="4550"/>
        <source>Switch Style to %1</source>
        <translation>Changer le style en %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1959"/>
        <location filename="../MainWindow.cpp" line="1984"/>
        <source>Edit record</source>
        <translation>Modifier l&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2088"/>
        <location filename="../MainWindow.cpp" line="4568"/>
        <location filename="../MainWindow.cpp" line="4572"/>
        <location filename="../MainWindow.cpp" line="4576"/>
        <location filename="../MainWindow.h" line="166"/>
        <location filename="../MainWindow.h" line="170"/>
        <location filename="../MainWindow.h" line="174"/>
        <source>Median</source>
        <translation>Médian</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2090"/>
        <source>Click to swap Average and Median</source>
        <translation>Cliquer pour échanger moyenne et médiane</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2185"/>
        <source>Click to swap Legend and Label</source>
        <translation>Cliquer pour échanger la légende et le label</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2262"/>
        <source>No records to preview for selected time range!</source>
        <translation>Aucun enregistrement à prévisualiser pour la plage de temps sélectionnée&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2294"/>
        <source>No records to print for selected time range!</source>
        <translation>Aucun enregistrement à imprimer pour la plage de temps sélectionnée&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2340"/>
        <location filename="../MainWindow.cpp" line="2390"/>
        <location filename="../MainWindow.cpp" line="2466"/>
        <location filename="../MainWindow.cpp" line="2597"/>
        <source>User %1</source>
        <translation>Utilisateur %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>DATE</source>
        <translation>DATE</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>TIME</source>
        <translation>TEMPS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>SYS</source>
        <translation>SYS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>DIA</source>
        <translation>DIA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>BPM</source>
        <translation>BPM</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>IHB</source>
        <translation>IHB</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>COMMENT</source>
        <translation>COMMENTAIRE</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>PPR</source>
        <translation>PPR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2502"/>
        <source>MOV</source>
        <translation>MOUV</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2662"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation>Impossible de créer un courriel car la génération de la base64 pour la pièce jointe «&#xa0;%1&#xa0;» a échoué&#xa0;!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3413"/>
        <location filename="../MainWindow.cpp" line="4164"/>
        <source>Chart</source>
        <translation>Graphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3413"/>
        <location filename="../MainWindow.cpp" line="4181"/>
        <source>Table</source>
        <translation>Tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3413"/>
        <location filename="../MainWindow.cpp" line="4198"/>
        <source>Statistic</source>
        <translation>Statistique</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4122"/>
        <source>No records to analyse!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4134"/>
        <source>No records to display for selected time range!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4216"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation>Impossible d&apos;ouvrir le courriel «&#xa0;%1&#xa0;»&#xa0;!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4226"/>
        <source>Could not start e-mail client!</source>
        <translation>Impossible de lancer le client de messagerie&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4231"/>
        <source>No records to e-mail for selected time range!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4237"/>
        <source>Select Icon Color</source>
        <translation>Sélectionnez la couleur de l&apos;icône</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4243"/>
        <source>Restart application to apply new icon color.</source>
        <translation>Redémarrez l&apos;application pour appliquer la nouvelle couleur de l&apos;icône.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5591"/>
        <source>The database can not be saved.

Please choose another location in the settings or all data will be lost.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2334"/>
        <location filename="../MainWindow.cpp" line="2384"/>
        <location filename="../MainWindow.cpp" line="2460"/>
        <location filename="../MainWindow.cpp" line="2591"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation>Crée avec UBPM pour
Windows / Linux / macOS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2335"/>
        <location filename="../MainWindow.cpp" line="2385"/>
        <location filename="../MainWindow.cpp" line="2461"/>
        <location filename="../MainWindow.cpp" line="2592"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Logiciel libre et gratuit
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2744"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Importer depuis CSV/XML/JSON/SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2744"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>Fichier CSV (*.csv);;Fichier XML (*.xml);;FichierJSON (*.json);;Fichier SQL (*.sql)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2800"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Impossible d&apos;ouvrir «&#xa0;%1&#xa0;»&#xa0;!

Raison&#xa0;: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1128"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation>Mesures&#xa0;: %1 | Irrégulier&#xa0;: %2 | Mouvement&#xa0;: %3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="651"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Cher Dr NomduPraticien,

veuillez trouver ci-joint les données de ma tension artérielle pour ce mois-ci.

Meilleures salutations,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1133"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation>Mesures : 0 | Irrégulier : 0 | Mouvement : 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2340"/>
        <location filename="../MainWindow.cpp" line="2390"/>
        <location filename="../MainWindow.cpp" line="2466"/>
        <location filename="../MainWindow.cpp" line="2597"/>
        <source>%1 (Age: %2, Height: %3cm, Weight: %4Kg, BMI: %5)</source>
        <translation>%1 (Âge : %2, Taille : %3cm, Poids : %4Kg, BMI : %5)</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2782"/>
        <location filename="../MainWindow.cpp" line="4015"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform>Importation réussie de %n enregistrement depuis %1.

     Utilisateur 1 : %2
     Utilisateur 2 : %3</numerusform>
            <numerusform>Importation réussie de %n enregistrements depuis %1.

     Utilisateur 1 : %2
     Utilisateur 2 : %3</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2786"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>Saut de %n enregistrement invalide&#xa0;!</numerusform>
            <numerusform>Saut de %n enregistrements invalides&#xa0;!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2791"/>
        <location filename="../MainWindow.cpp" line="4019"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>Sauter %n enregistrement en double&#xa0;!</numerusform>
            <numerusform>Sauter %n enregistrements en double&#xa0;!</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3170"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>Cela ne ressemble pas à une base de données UBPM&#xa0;!

Peut-être de mauvais paramètres de cryptage/mot de passe&#xa0;?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3223"/>
        <location filename="../MainWindow.cpp" line="3227"/>
        <source>Export to %1</source>
        <translation>Exporter vers %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3223"/>
        <location filename="../MainWindow.cpp" line="3227"/>
        <source>%1 File (*.%2)</source>
        <translation>%1 Fichier (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3263"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>Impossible de créer &quot;%1&quot; !

Motif : %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3269"/>
        <source>The database is empty, no records to export!</source>
        <translation>La base de données est vide, pas d&apos;enregistrement à exporter&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3620"/>
        <source>Morning</source>
        <translation>Matin</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3620"/>
        <source>Afternoon</source>
        <translation>Après-midi</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3659"/>
        <source>Week</source>
        <translation>Semaine</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3714"/>
        <source>Quarter</source>
        <translation>Trimestre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3743"/>
        <source>Half Year</source>
        <translation>Semestre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3761"/>
        <source>Year</source>
        <translation>Année</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3949"/>
        <source>Really delete all records for user %1?</source>
        <translation>Supprimer réellement tous les enregistrements de l&apos;utilisateur %1 ?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3964"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Tous les enregistrements de l&apos;utilisateur %1 sont supprimés et la base de données existante est sauvegardée dans ubpm.sql.bak.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4089"/>
        <source>Really delete all records?</source>
        <translation>Vraiment effacer tous les enregistrements&#xa0;?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4100"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Tous les enregistrements sont supprimés et la base de données existante ubpm.sql est mise à la poubelle.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4406"/>
        <source>- UBPM Application
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4421"/>
        <source>- UBPM Plugins
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4565"/>
        <location filename="../MainWindow.cpp" line="4569"/>
        <location filename="../MainWindow.cpp" line="4573"/>
        <location filename="../MainWindow.h" line="163"/>
        <location filename="../MainWindow.h" line="167"/>
        <location filename="../MainWindow.h" line="171"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4566"/>
        <location filename="../MainWindow.cpp" line="4570"/>
        <location filename="../MainWindow.cpp" line="4574"/>
        <location filename="../MainWindow.h" line="164"/>
        <location filename="../MainWindow.h" line="168"/>
        <location filename="../MainWindow.h" line="172"/>
        <source>Maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4639"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>Impossible d&apos;ouvrir le fichier du thème «&#xa0;%1&#xa0;»&#xa0;!

Motif&#xa0;: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1956"/>
        <location filename="../MainWindow.cpp" line="1973"/>
        <location filename="../MainWindow.cpp" line="5473"/>
        <source>Delete record</source>
        <translation>Effacer l&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5400"/>
        <source>Show Heartrate</source>
        <translation>Afficher la fréquence cardiaque</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5436"/>
        <location filename="../MainWindow.cpp" line="5447"/>
        <source>Symbols and lines can&apos;t be disabled both!</source>
        <translation>Les symboles et les lignes ne peuvent pas être désactivés tous les deux&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5475"/>
        <source>Show record</source>
        <translation>Afficher l&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1957"/>
        <location filename="../MainWindow.cpp" line="1980"/>
        <location filename="../MainWindow.cpp" line="5476"/>
        <source>Hide record</source>
        <translation>Masquer l&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1975"/>
        <location filename="../MainWindow.cpp" line="5487"/>
        <source>Really delete selected record?</source>
        <translation>Vraiment effacer les enregistrements sélectionnés&#xa0;?</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="4518"/>
        <source>The following %n translation(s) for &quot;%1&quot; could not be loaded:

%2
Hide this warning on program startup?</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5369"/>
        <source>Scrolling has reached top/bottom, show next/previous period?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5392"/>
        <source>Dynamic Scaling</source>
        <translation>Mise à l&apos;échelle dynamique</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5393"/>
        <source>Colored Stripes</source>
        <translation>Rayures colorées</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5395"/>
        <source>Show Symbols</source>
        <translation>Afficher les symboles</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5396"/>
        <source>Show Lines</source>
        <translation>Afficher les lignes</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5398"/>
        <source>Colored Symbols</source>
        <translation>Symboles colorés</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5508"/>
        <source>Show Median</source>
        <translation>Afficher la médiane</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5509"/>
        <source>Show Values</source>
        <translation>Afficher les valeurs</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5560"/>
        <source>Really quit program?</source>
        <translation>Vraiment quitter le programme&#xa0;?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Gestionnaire universel de la pression artérielle</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="13"/>
        <source>The application is already running and multiple instances are not allowed.

Switch to the running instance or close it and try again.</source>
        <translation>L&apos;application est déjà en cours d&apos;exécution et les instances multiples ne sont pas autorisées.

Passez à l&apos;instance en cours ou fermez-la et réessayez.</translation>
    </message>
</context>
</TS>
