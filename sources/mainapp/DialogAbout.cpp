#include "DialogAbout.h"

DialogAbout::DialogAbout(QWidget *parent) : QDialog(parent)
{
	QString externalTranslations = QFileInfo(reinterpret_cast<MainWindow*>(parent)->translatorQtBase.filePath()).absolutePath();

	setupUi(this);

	layout()->setSizeConstraint(QLayout::SetFixedSize);

	label_ubpm->setText(QString("%1 [ %2 ]").arg(APPVERS, APPDATE));
	label_qt->setText(QT_VERSION_STR);
	label_os->setText(QSysInfo::prettyProductName());
	label_license->setText(tr("The program is provided as is with no warranty of any kind, including the warranty of design, merchantability and fitness for a particular purpose."));

	label_settings->setText(QString("<a href=\"file://%1\">%1</a>").arg(reinterpret_cast<MainWindow*>(parent)->envSettings));
	label_database->setText(QString("<a href=\"file://%1\">%1</a>").arg(reinterpret_cast<MainWindow*>(parent)->envDatabase));
	label_cache->setText(QString("<a href=\"file://%1\">%1</a>").arg(QStandardPaths::writableLocation(QStandardPaths::CacheLocation)));
	label_guides->setText(QString("<a href=\"file://%1\">%1</a>").arg(reinterpret_cast<MainWindow*>(parent)->envGuides));
	label_languages->setText(QString("<a href=\"file://%1\">%1</a>%2").arg(reinterpret_cast<MainWindow*>(parent)->envLanguages, reinterpret_cast<MainWindow*>(parent)->envLanguages.contains(externalTranslations) ? "" : QString("<br><a href=\"file://%1\">%1</a>").arg(externalTranslations)));
	label_plugins->setText(QString("<a href=\"file://%1\">%1</a>").arg(reinterpret_cast<MainWindow*>(parent)->envPlugins));
	label_themes->setText(QString("<a href=\"file://%1\">%1</a>").arg(reinterpret_cast<MainWindow*>(parent)->envThemes));
}
