#ifndef DLGABOUT_H
#define DLGABOUT_H

#include "MainWindow.h"
#include "ui_DialogAbout.h"

class DialogAbout : public QDialog, private Ui::DialogAbout
{
	Q_OBJECT

public:

	explicit DialogAbout(QWidget*);
};

#endif // DLGABOUT_H
