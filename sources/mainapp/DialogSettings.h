#ifndef DLGSETTINGS_H
#define DLGSETTINGS_H

#include "MainWindow.h"
#include "ui_DialogSettings.h"

enum { TAB_DATABASE, TAB_USER, TAB_DEVICE, TAB_CHART, TAB_TABLE, TAB_STATS, TAB_EMAIL, TAB_PLUGIN, TAB_UPDATE };

class DialogSettings : public QDialog, private Ui::DialogSettings
{
	Q_OBJECT

public:

	explicit DialogSettings(QWidget*, struct SETTINGS*, QVector <QPluginLoader*>, int*);

private:

	struct SETTINGS *settings;

	QVector <QPluginLoader*> plugins;
	DeviceInterface *deviceInterface;
	DEVICEINFO deviceInfo;
	int *plugin;

	bool settingsUnchanged();
	void changeDatabase();

private slots:

	void on_comboBox_backup_mode_currentIndexChanged(int);

	void on_checkBox_heartrate_toggled(bool);
	void on_checkBox_autostart_toggled(bool);

	void on_comboBox_plugins_currentIndexChanged(int);

	void on_toolButton_choose_clicked();
	void on_toolButton_backup_choose_clicked();
	void on_toolButton_view_pressed();
	void on_toolButton_view_released();

	void on_horizontalSlider_symbolsize_valueChanged(int);
	void on_horizontalSlider_linewidth_valueChanged(int);
	void on_horizontalSlider_sys1_valueChanged(int);
	void on_horizontalSlider_dia1_valueChanged(int);
	void on_horizontalSlider_ppr1_valueChanged(int);
	void on_horizontalSlider_bpm1_valueChanged(int);
	void on_horizontalSlider_sys2_valueChanged(int);
	void on_horizontalSlider_dia2_valueChanged(int);
	void on_horizontalSlider_ppr2_valueChanged(int);
	void on_horizontalSlider_bpm2_valueChanged(int);

	void on_pushButton_save_clicked();
	void on_pushButton_reset_clicked();
	void on_pushButton_close_clicked();

	void keyPressEvent(QKeyEvent*);

	void reject();
};

#endif // DLGSETTINGS_H
