#ifndef DLGDONATION_H
#define DLGDONATION_H

#include "MainWindow.h"
#include "ui_DialogDonation.h"

class DialogDonation : public QDialog, private Ui::DialogDonation
{
	Q_OBJECT

public:

	explicit DialogDonation(QWidget*);

private slots:

	void on_pushButton_amazon_clicked();
	void on_pushButton_paypal_account_clicked();
	void on_pushButton_paypal_creditcard_clicked();
	void on_pushButton_sepa_name_clicked();
	void on_pushButton_sepa_iban_clicked();
	void on_pushButton_sepa_bic_clicked();

	void keyPressEvent(QKeyEvent*);
};

#endif // DLGDONATION_H
