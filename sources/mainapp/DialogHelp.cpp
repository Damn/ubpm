#include "DialogHelp.h"

DialogHelp::DialogHelp(QWidget *parent, QString lng, QString guides, struct SETTINGS *psettings) : QDialog(parent)
{
	setupUi(this);

	setWindowFlags(Qt::Window | Qt::WindowMinimizeButtonHint  | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);

	settings = psettings;
	requested = lng;
	dir = guides;

	if(QFile::exists(QString("%1/%2.qhc").arg(dir, lng)))
	{
		language = lng;
		fallback = false;
	}
	else
	{
		language = "en";
		fallback = true;
	}

	QFile::copy(QString("%1/%2.qhc").arg(dir, language), QString("%1/%2.qhc").arg(QStandardPaths::writableLocation(QStandardPaths::CacheLocation), language));
	QFile::copy(QString("%1/%2.qch").arg(dir, language), QString("%1/%2.qch").arg(QStandardPaths::writableLocation(QStandardPaths::CacheLocation), language));

	helpEngine = new QHelpEngine(QString("%1/%2.qhc").arg(QStandardPaths::writableLocation(QStandardPaths::CacheLocation), language));
	helpEngine->setupData();

	contentWidget = helpEngine->contentWidget();
	contentWidget->setMinimumWidth(300);

	verticalLayout->addWidget((QWidget*)contentWidget);

	helpBrowser->setHelpEngine(helpEngine);

	splitter->setStretchFactor(0, 0);
	splitter->setStretchFactor(1, 1);

	connect(contentWidget, &QHelpContentWidget::clicked, this, &DialogHelp::setSourceFromContent);
	connect(contentWidget, &QHelpContentWidget::activated, this, &DialogHelp::setSourceFromContent);
	connect(helpBrowser, &HelpBrowser::anchorClicked, this, &DialogHelp::anchorClicked);
}

void DialogHelp::showHelp(QString page)
{
	if(!initialized)
	{
		if(fallback)
		{
			if(!QFile::exists(QString("%1/en.qhc").arg(dir)))
			{
				QMessageBox::warning(nullptr, APPNAME, tr("%1 guide not found!").arg(language.toUpper()));

				close();

				return;
			}
			else
			{
				QMessageBox::information(nullptr, APPNAME, tr("%1 guide not found, showing EN guide instead.").arg(requested.toUpper()));
			}
		}

		contentWidget->expandAll();

		initialized = true;
	}

	setSourceFromPage(page);

	if(isHidden())
	{
		restoreGeometry(settings->help.geometry);

		show();
	}

	raise();
}

QString DialogHelp::getSource()
{
	return helpBrowser->source().toString();
}

void DialogHelp::setSource(QUrl url)
{
	helpBrowser->setSource(url);

	contentWidget->setCurrentIndex(contentWidget->indexOf(url));
}
void DialogHelp::setSourceFromPage(QString page)
{
	setSource(QString("qthelp://page.codeberg.lazyt.ubpm/%1/%2.html").arg(language, page));
}

void DialogHelp::setSourceFromContent(QModelIndex index)
{
	setSource(helpEngine->contentModel()->contentItemAt(index)->url());
}

void DialogHelp::anchorClicked(QUrl link)
{
	setSource(link);
}

void DialogHelp::reject()
{
	settings->help.geometry = saveGeometry();

	hide();
}

DialogHelp::~DialogHelp()
{
	delete helpEngine;

	QFile::remove(QString("%1/%2.qch").arg(QStandardPaths::writableLocation(QStandardPaths::CacheLocation), language));
	QFile::remove(QString("%1/%2.qhc").arg(QStandardPaths::writableLocation(QStandardPaths::CacheLocation), language));
}
