#ifndef DLGIMPORT_H
#define DLGIMPORT_H

#define VID 0x04D9
#define PID 0xB534

#define MAINTAINER	QString("<a href='mailto:lazyt@mailbox.org?subject=UBPM Plugin %1'>Thomas Löwe</a>%2").arg(MODEL, HELPER)
#define VERSION		QString("1.4.0 [ USB2SERIAL ]")
#define PRODUCER	"<a href='https://www.veroval.info/en/products/bloodpressure'>Hartmann</a>"
#define MODEL		"DC318"
#define ALIAS		"Veroval Duo Control"
#define HELPER		", Andreas Hencke"
#define ICON		":/plugin/svg/usb-srl.svg"

#define TIMEOUT 1000

#include "ui_DialogImport.h"

#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <QStandardPaths>
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QThread>
#include <QTimer>
#include <QTranslator>

#include "deviceinterface.h"

class DialogImport : public QDialog, private Ui::DialogImport
{
	Q_OBJECT

public:

	explicit DialogImport(QWidget*, QString, QVector <struct HEALTHDATA>*, QVector <struct HEALTHDATA>*, struct SETTINGS*);

	bool failed = false;

private:

	QByteArray cmd_mrn1 = QByteArray::fromHex("023F4D524E3103");
	QByteArray cmd_mrn2 = QByteArray::fromHex("023F4D524E3203");
	QByteArray cmd_mdr1 = QByteArray::fromHex("023F4D4452314103");
	QByteArray cmd_mdr2 = QByteArray::fromHex("023F4D4452324103");
	QByteArray cmd_data = QByteArray::fromHex("05");

	QSerialPortInfo spi;
	QSerialPort sp;

	QByteArray payloads[2];

	QFile log;

	QVector <struct HEALTHDATA> *u1, *u2;

	struct SETTINGS *settings;

	int cnt1, cnt2;

	bool abort = false;
	bool finished = true;

	bool sendCMD(QByteArray);
	void decryptPayload();
	void logRawData(bool, int, QByteArray);

private slots:

	void on_comboBox_activated(int);

	void on_checkBox_auto_import_toggled(bool);

	void on_toolButton_toggled(bool);

	void on_pushButton_import_clicked();
	void on_pushButton_cancel_clicked();

	void reject();
};

#endif // DLGIMPORT_H
