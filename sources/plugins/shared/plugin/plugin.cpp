#include "plugin.h"

DEVICEINFO DevicePlugin::getDeviceInfo()
{
	return DEVICEINFO { PRODUCER, MODEL, ALIAS, MAINTAINER, VERSION, ICON };
}

bool DevicePlugin::getDeviceData(QWidget *parent, QString theme, QVector <struct HEALTHDATA> *user1, QVector <struct HEALTHDATA> *user2, struct SETTINGS *settings)
{
	DialogImport dlg(parent, theme, user1, user2, settings);

	if(!dlg.failed)
	{
		return dlg.exec();
	}

	return QDialog::Rejected;
}
