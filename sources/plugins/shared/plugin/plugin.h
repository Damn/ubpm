#ifndef DEVICEPLUGIN_H
#define DEVICEPLUGIN_H

#include <QtPlugin>
#include <QObject>

#include "DialogImport.h"

class DevicePlugin : public QObject, DeviceInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "page.codeberg.lazyt.ubpm.deviceinterface")
	Q_INTERFACES(DeviceInterface)

public:

	DEVICEINFO getDeviceInfo();
	bool getDeviceData(QWidget*, QString, QVector <struct HEALTHDATA>*, QVector <struct HEALTHDATA>*, struct SETTINGS*);
};

#endif // DEVICEPLUGIN_H
