<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>DialogImport</name>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="14"/>
        <source>Device Import</source>
        <translation>Importazione Dispositivo</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="84"/>
        <source>Plugin Options</source>
        <translation>Opzioni Plugin</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="24"/>
        <source>Bluetooth Controller</source>
        <translation>Controller Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="110"/>
        <source>Import Measurements automatically</source>
        <translation>Importa misurazioni automaticamente</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="30"/>
        <source>Select Bluetooth Controller</source>
        <translation>Seleziona Controller Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="37"/>
        <source>Discover Device</source>
        <translation>Scopri Dispositivo</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="120"/>
        <source>Device Information</source>
        <translation>Informazioni sul Dispositivo</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="51"/>
        <source>Bluetooth Device</source>
        <translation>Dispositivo Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="60"/>
        <source>Select Bluetooth Device</source>
        <translation>Seleziona Dispositivo Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="69"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="69"/>
        <source>Serial</source>
        <translation>Seriale</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="70"/>
        <source>Connect Device</source>
        <translation>Connetti Dispositivo</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="62"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="62"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="76"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="76"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="156"/>
        <source>Producer</source>
        <translation>Produttore</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="83"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="83"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="163"/>
        <source>Product</source>
        <translation>Prodotto</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="55"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="55"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="86"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="90"/>
        <source>Discover Device automatically</source>
        <translation>Scopri Dispositivo automaticamente</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="103"/>
        <source>Connect Device automatically</source>
        <translation>Connetti il Dispositivo automaticamente</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="158"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="182"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="111"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="135"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="188"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="135"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="173"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="132"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="262"/>
        <source>Import</source>
        <translation>Importa</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="175"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="199"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="128"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="152"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="205"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="152"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="190"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="149"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="279"/>
        <source>Write Logfile</source>
        <translation>Scrivi file di registro</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="149"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="201"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="225"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="154"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="178"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="231"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="178"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="216"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="175"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="305"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="24"/>
        <source>Could not find a serial port.

Is the usb2serial driver installed and the device connected?</source>
        <translation>Impossibile trovare una porta seriale.

Il driver seriale usb2 è installato e il dispositivo è collegato?</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="76"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="76"/>
        <source>Could not read data.

%1</source>
        <translation>Impossibile leggere i dati.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="85"/>
        <source>The device doesn&apos;t respond.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="212"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="253"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="244"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="286"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="203"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="240"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="382"/>
        <source>The import was canceled.</source>
        <translation>L&apos;importazione è stata annullata.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="102"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="102"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="75"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="159"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="112"/>
        <source>Could not write data.

%1</source>
        <translation>Impossibile scrivere i dati.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="85"/>
        <source>The device doesn&apos;t respond.

Press &quot;MEM&quot;, select U1/U2 with &quot;POWER&quot; and wait until the user data is shown.

Then try again…</source>
        <translation>Il dispositivo non risponde.

Premere &quot;MEM&quot;, selezionare U1/U2 con &quot;POWER&quot; e attendere che vengano visualizzati i dati utente.

Poi riprova…</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="115"/>
        <source>Was &quot;U1&quot; selected on the device?

For &quot;U2&quot; answer with No.</source>
        <translation>È stato selezionato &quot;U1&quot; sul dispositivo?

Per &quot;U2&quot; rispondi con No.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="180"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="200"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="173"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="198"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="252"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="187"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="273"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="177"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="659"/>
        <source>Could not open the logfile %1.

%2</source>
        <translation>Impossibile aprire il file di registro %1.

%2</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="316"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="270"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="318"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="222"/>
        <source>Could not open serial port &quot;%1&quot;.

%2</source>
        <translation>Impossibile aprire la porta seriale &quot;%1&quot;.

%2</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="190"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="183"/>
        <source>The device doesn&apos;t respond.

Disconnect the USB cable and reconnect it.

Then try again…</source>
        <translation>Il dispositivo non risponde.

Scollegare il cavo USB e ricollegarlo.

Poi riprova…</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="201"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="242"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="192"/>
        <source>Could not read measurement count.</source>
        <translation>Impossibile leggere il conteggio delle misurazioni.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="223"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="264"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="214"/>
        <source>Could not read measurement %1.</source>
        <translation>Impossibile leggere la misurazione %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="286"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="330"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="236"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="284"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="682"/>
        <source>Cancel import?</source>
        <translation>Annullare l&apos;importazione?</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="297"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="341"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="295"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="343"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="482"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="244"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="693"/>
        <source>Import in progress…</source>
        <translation>Importazione in corso…</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="262"/>
        <source>No answer from the device.

Did you use the Bluetooth controller with the cloned MAC address?</source>
        <translation>Nessuna risposta dal dispositivo.

Hai usato il controller Bluetooth con l&apos;indirizzo MAC clonato?</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="280"/>
        <source>Can&apos;t set device into info mode.</source>
        <translation>Impossibile impostare il dispositivo in modalità informazioni.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="293"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="307"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="332"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="349"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="366"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="403"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="442"/>
        <source>Send %1 command failed.</source>
        <translation>Invio %1 comando non riuscito.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="319"/>
        <source>Can&apos;t set device into data mode.</source>
        <translation>Impossibile impostare il dispositivo in modalità dati.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="117"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="70"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="387"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="429"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="202"/>
        <source>Import aborted by user.</source>
        <translation>Importazione interrotta dall&apos;utente.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="152"/>
        <source>Acknowledge failed.</source>
        <translation>Riconoscimento fallito.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="84"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="100"/>
        <source>Could not read data.

Is the device powered on?

%1</source>
        <translation>Impossibile leggere i dati.

Il dispositivo è acceso?

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="38"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="38"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="45"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="40"/>
        <source>Could not open usb device %1:%2.</source>
        <translation>Impossibile aprire il dispositivo USB %1:%2.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="58"/>
        <source>No Bluetooth controller found.</source>
        <translation>Nessun controller Bluetooth trovato.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="107"/>
        <source>Bluetooth error.

%1</source>
        <translation>Errore Bluetooth.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="122"/>
        <source>No device discovered.

Check the connection in operating system or press Bluetooth button on device and try again…</source>
        <translation>Nessun dispositivo rilevato.

Verificare la connessione nel sistema operativo o premere il pulsante Bluetooth sul dispositivo e riprovare…</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="154"/>
        <source>The selected device is not a %1.</source>
        <translation>Il dispositivo selezionato non è un %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="163"/>
        <source>Device doesn&apos;t respond, try again?</source>
        <translation>Il dispositivo non risponde, riprovare?</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="177"/>
        <source>Could not connect to the device.

Check the connection in operating system or press Bluetooth button on device and try again…

%1</source>
        <translation>Impossibile connettersi al dispositivo.

Verificare la connessione nel sistema operativo o premere il pulsante Bluetooth sul dispositivo e riprovare...

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="187"/>
        <source>Press START/STOP on device and try again…</source>
        <translation>Premi START/STOP sul dispositivo e riprova…</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="332"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="236"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="471"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="233"/>
        <source>Really abort import?</source>
        <translation>Interrompere davvero l&apos;importazione?</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="431"/>
        <source>Could not access the Bluetooth service %1.</source>
        <translation>Impossibile accedere al servizio Bluetooth %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="607"/>
        <source>The selected Bluetooth controller is not available.</source>
        <translation>Il controller Bluetooth selezionato non è disponibile.</translation>
    </message>
</context>
</TS>
