<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">

	<id>page.codeberg.lazyt.ubpm</id>

	<metadata_license>FSFAP</metadata_license>
	<project_license>GPL-3.0+</project_license>

	<name>Universal Blood Pressure Manager</name>
	<name xml:lang="de">Universeller Blutdruck Manager</name>

	<summary>Manage blood pressure data</summary>
	<summary xml:lang="de">Blutdruckdaten verwalten</summary>

	<description>
		<p>A program to manage your blood pressure data locally and privately, without the constraints of the manufacturer's cloud.</p>
		<p xml:lang="de">Ein Programm zur lokalen und privaten Verwaltung Ihrer Blutdruckdaten, ohne die Zwänge der Hersteller-Cloud.</p>

		<p>The following features are implemented:</p>
		<p xml:lang="de">Die folgenden Funktionen sind implementiert:</p>

		<ul>
			<li>enter data manually or read directly from supported blood pressure monitors via cable or Bluetooth</li>
			<li xml:lang="de">Daten eingeben oder direkt von unterstützten Blutdruckmessgeräten per Kabel oder Bluetooth einlesen</li>

			<li>migrate existing data from manufacturer's software</li>
			<li xml:lang="de">Daten von der Software des Herstellers migrieren</li>

			<li>import/export data from/to csv, json, xml, pdf and sqlite</li>
			<li xml:lang="de">Daten von/nach csv, json, xml, pdf und sqlite importieren/exportieren</li>

			<li>view data as graph, table or statistic</li>
			<li xml:lang="de">Daten als Diagramm, Tabelle oder Statistik anzeigen</li>

			<li>analyse data with SQL queries</li>
			<li xml:lang="de">Daten mittels SQL-Abfragen analysieren</li>

			<li>print data or e-mail it directly to the doctor</li>
			<li xml:lang="de">Daten ausdrucken oder per E-Mail direkt an den Arzt senden</li>
		</ul>

		<p>And much more...</p>
		<p xml:lang="de">Und vieles mehr...</p>
	</description>

	<categories>
		<category>Science</category>
		<category>MedicalSoftware</category>
		<category>DataVisualization</category>
		<category>Qt</category>
	</categories>

	<keywords>
		<keyword>ubpm</keyword>
		<keyword>blood pressure</keyword>
		<keyword xml:lang="de">Blutdruck</keyword>
	</keywords>

	<screenshots>
		<screenshot type="default">
			<caption>Chart View</caption>
			<image>https://codeberg.org/lazyt/ubpm/raw/branch/master/website/chartview.png</image>
		</screenshot>
		<screenshot>
			<caption>Table View</caption>
			<image>https://codeberg.org/lazyt/ubpm/raw/branch/master/website/tableview.png</image>
		</screenshot>
		<screenshot>
			<caption>Stats View</caption>
			<image>https://codeberg.org/lazyt/ubpm/raw/branch/master/website/statsview.png</image>
		</screenshot>
		<screenshot>
			<caption>Guide</caption>
			<image>https://codeberg.org/lazyt/ubpm/raw/branch/master/website/guide.png</image>
		</screenshot>
	</screenshots>

	<url type="homepage">https://codeberg.org/LazyT/ubpm#ubpm-universal-blood-pressure-manager</url>
	<url type="bugtracker">https://codeberg.org/lazyt/ubpm/issues</url>
	<url type="help">https://codeberg.org/LazyT/ubpm/wiki</url>
	<url type="donation">https://www.paypal.com/donate/?hosted_button_id=VT55E55UYP3VA</url>
	<url type="translate">https://hosted.weblate.org/projects/ubpm</url>
	<url type="vcs-browser">https://codeberg.org/LazyT/ubpm/src/branch/master/sources</url>

	<icon type="remote">https://codeberg.org/LazyT/ubpm/raw/branch/master/sources/mainapp/res/ico/ubpm.png</icon>

	<developer_name>Thomas "LazyT" Löwe</developer_name>

	<launchable type="desktop-id">ubpm.desktop</launchable>

	<recommends>
		<control>keyboard</control>
		<control>pointing</control>
		<display_length compare="ge">medium</display_length>
	</recommends>

	<content_rating type="oars-1.1">
		<content_attribute id="social-info">mild</content_attribute>
		<content_attribute id="money-purchasing">mild</content_attribute>
	</content_rating>

	<provides>
		<id>ubpm.desktop</id>
	</provides>

	<releases>
		<release version="1.9.0" date="2023-09-11"/>
		<release version="1.8.0" date="2023-08-06"/>
		<release version="1.7.4" date="2023-07-07"/>
		<release version="1.7.3" date="2023-03-04"/>
		<release version="1.7.2" date="2022-10-22"/>
		<release version="1.7.1" date="2022-09-20"/>
		<release version="1.7.0" date="2022-06-11"/>
		<release version="1.6.2" date="2022-04-30"/>
		<release version="1.6.1" date="2022-03-19"/>
		<release version="1.6.0" date="2022-01-01"/>
		<release version="1.5.0" date="2021-11-20"/>
		<release version="1.4.0" date="2021-10-23"/>
		<release version="1.3.0" date="2021-09-25"/>
		<release version="1.2.0" date="2021-08-28"/>
		<release version="1.1.0" date="2021-07-31"/>
		<release version="1.0.6" date="2021-07-07"/>
		<release version="1.0.5" date="2021-06-07"/>
		<release version="1.0.4" date="2021-05-08"/>
		<release version="1.0.3" date="2021-04-10"/>
		<release version="1.0.2" date="2021-03-19"/>
		<release version="1.0.1" date="2021-02-27"/>
		<release version="1.0.0" date="2021-02-11"/>
	</releases>

</component>
